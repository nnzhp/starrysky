define({ "api": [
  {
    "type": "delete",
    "url": "/api/chameleon/interface",
    "title": "10、删除接口",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "DeleteApiChameleonInterface",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/api/chameleon/interface_category",
    "title": "6、删除接口分类",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "DeleteApiChameleonInterface_category",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface_category"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/chameleon/interface",
    "title": "7、获取接口列表",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "20",
            "description": "<p>每页数量</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>第几页</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>模糊搜索</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "category",
            "description": "<p>分类id，根据分类筛选</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>总数量</p>"
          },
          {
            "group": "返回参数",
            "type": "List",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>接口id</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>接口名称，唯一</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.desc",
            "description": "<p>接口描述</p>"
          },
          {
            "group": "返回参数",
            "type": "Json",
            "optional": false,
            "field": "data.category",
            "description": "<p>接口归属分类，具体字段见接口分类返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.url",
            "description": "<p>接口url</p>"
          },
          {
            "group": "返回参数",
            "type": "Json",
            "optional": false,
            "field": "data.method",
            "description": "<p>接口请求方式</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.method.name",
            "description": "<p>接口请求方式名称</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.method.value",
            "description": "<p>接口请求方式id</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.response",
            "description": "<p>接口返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.response_header",
            "description": "<p>接口响应头</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.code",
            "description": "<p>接口自定义代码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.proxy_url",
            "description": "<p>转发url</p>"
          },
          {
            "group": "返回参数",
            "type": "Boolean",
            "optional": false,
            "field": "data.proxy_tag",
            "description": "<p>是否转发开关</p>"
          },
          {
            "group": "返回参数",
            "type": "Boolean",
            "optional": false,
            "field": "data.status",
            "description": "<p>接口状态</p>"
          },
          {
            "group": "返回参数",
            "type": "json",
            "optional": false,
            "field": "data.create_user",
            "description": "<p>创建用户，具体字段见用户信息返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "json",
            "optional": false,
            "field": "data.update_user",
            "description": "<p>修改用户，，具体字段见用户信息返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.create_time",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.update_time",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\": 0,\n    \"msg\": \"操作成功\",\n    \"data\": [{\n            \"id\": 41,\n            \"create_time\": \"2021-08-31 16:53:28\",\n            \"update_time\": \"2021-08-31 16:55:57\",\n            \"name\": \"fmz_fcx_0\",\n            \"desc\": null,\n            \"url\": \"/fcx/interface_0\",\n            \"category\": {\n                \"id\": 1,\n                \"create_time\": \"2021-08-19 00:24:50\",\n                \"update_time\": \"2021-08-19 00:24:53\",\n                \"name\": \"用户中心\",\n                \"prefix\": \"usercenter\",\n                \"create_user\": {\n                    \"id\": 1,\n                    \"create_time\": \"2021-08-18 14:01:49\",\n                    \"update_time\": \"2021-08-20 23:42:46\",\n                    \"phone\": \"18612532945\",\n                    \"email\": \"admin@qq.com\",\n                    \"password\": \"7da387a4cf65ab80356f8e4e38f1c290\",\n                    \"nick\": \"水壶\",\n                    \"avatar\": \"static/avatar/default.jpg\",\n                    \"roles\": [{\n                        \"id\": 3,\n                        \"create_time\": \"2021-08-20 23:40:55\",\n                        \"update_time\": \"2021-08-20 23:40:55\",\n                        \"name\": \"管理员\",\n                        \"alias\": \"admin\"\n                    }],\n                    \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/default.jpg\"\n                },\n                \"update_user\": {\n                    \"id\": 1,\n                    \"create_time\": \"2021-08-18 14:01:49\",\n                    \"update_time\": \"2021-08-20 23:42:46\",\n                    \"phone\": \"18612532945\",\n                    \"email\": \"admin@qq.com\",\n                    \"password\": \"7da387a4cf65ab80356f8e4e38f1c290\",\n                    \"nick\": \"水壶\",\n                    \"avatar\": \"static/avatar/default.jpg\",\n                    \"roles\": [{\n                        \"id\": 3,\n                        \"create_time\": \"2021-08-20 23:40:55\",\n                        \"update_time\": \"2021-08-20 23:40:55\",\n                        \"name\": \"管理员\",\n                        \"alias\": \"admin\"\n                    }],\n                    \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/default.jpg\"\n                }\n            },\n            \"method\": {\n                \"name\": \"post\",\n                \"value\": 1\n            },\n            \"response\": \"mock接口_fmz_0\",\n            \"response_header\": \"{}\",\n            \"code\": \"\",\n            \"proxy_url\": null,\n            \"proxy_tag\": false,\n            \"status\": true,\n            \"create_user\": {\n                \"id\": 2,\n                \"create_time\": \"2021-08-18 14:29:52\",\n                \"update_time\": \"2021-08-30 21:26:37\",\n                \"phone\": \"13810631245\",\n                \"email\": \"test1@qq.com\",\n                \"password\": \"7da387a4cf65ab80356f8e4e38f1c290\",\n                \"nick\": \"水果\",\n                \"avatar\": \"static/avatar/sgbg_83hH1ex.jpeg\",\n                \"roles\": [{\n                    \"id\": 3,\n                    \"create_time\": \"2021-08-20 23:40:55\",\n                    \"update_time\": \"2021-08-20 23:40:55\",\n                    \"name\": \"管理员\",\n                    \"alias\": \"admin\"\n                }],\n                \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/sgbg_83hH1ex.jpeg\"\n            },\n            \"update_user\": {\n                \"id\": 3,\n                \"create_time\": \"2021-08-18 14:31:47\",\n                \"update_time\": \"2021-08-20 23:42:58\",\n                \"phone\": \"13810631241\",\n                \"email\": \"test2@qq.com\",\n                \"password\": \"7da387a4cf65ab80356f8e4e38f1c290\",\n                \"nick\": \"风扇\",\n                \"avatar\": \"static/avatar/default.jpg\",\n                \"roles\": [{\n                    \"id\": 3,\n                    \"create_time\": \"2021-08-20 23:40:55\",\n                    \"update_time\": \"2021-08-20 23:40:55\",\n                    \"name\": \"管理员\",\n                    \"alias\": \"admin\"\n                }],\n                \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/default.jpg\"\n            }\n        }\n    }],\n\"count\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "GetApiChameleonInterface",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/chameleon/interface_category",
    "title": "3、获取接口分类列表",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "20",
            "description": "<p>每页数量</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>第几页</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>模糊搜索</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>总数量</p>"
          },
          {
            "group": "返回参数",
            "type": "List",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>分类id</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>分类名称，唯一</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.prefix",
            "description": "<p>分类前缀，唯一</p>"
          },
          {
            "group": "返回参数",
            "type": "json",
            "optional": false,
            "field": "data.create_user",
            "description": "<p>创建用户，具体字段见用户信息返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "json",
            "optional": false,
            "field": "data.update_user",
            "description": "<p>修改用户，，具体字段见用户信息返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.create_time",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.update_time",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "        {\n\"code\": 0,\n\"msg\": \"操作成功\",\n\"data\": [{\n\"id\": 5,\n\"create_time\": \"2021-08-31 20:08:38\",\n\"update_time\": \"2021-08-31 20:08:38\",\n\"name\": \"订单中心\",\n\"prefix\": \"order\",\n\"create_user\": {\n\"id\": 14,\n\"create_time\": \"2021-08-27 22:20:27\",\n\"update_time\": \"2021-08-27 22:20:27\",\n\"phone\": \"13812531232\",\n\"email\": \"admin1@qq.com\",\n\"password\": \"7da387a4cf65ab80356f8e4e38f1c290\",\n\"nick\": \"你是猪\",\n\"avatar\": \"static/avatar/aaa.jpeg\",\n\"roles\": [],\n\"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/aaa.jpeg\"\n},\n\"update_user\": {\n            \"id\": 14,\n            \"create_time\": \"2021-08-27 22:20:27\",\n            \"update_time\": \"2021-08-27 22:20:27\",\n            \"phone\": \"13812531232\",\n            \"email\": \"admin1@qq.com\",\n            \"password\": \"7da387a4cf65ab80356f8e4e38f1c290\",\n            \"nick\": \"你是猪\",\n            \"avatar\": \"static/avatar/aaa.jpeg\",\n            \"roles\": [],\n            \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/aaa.jpeg\"\n            }\n}],\n\"count\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "GetApiChameleonInterface_category",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface_category"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/chameleon/public_param",
    "title": "1、获取公共代码",
    "group": "变色龙",
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "List",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>参数id</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.code",
            "description": "<p>自定义代码</p>"
          },
          {
            "group": "返回参数",
            "type": "json",
            "optional": false,
            "field": "data.create_user",
            "description": "<p>创建用户，具体字段见用户信息返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "json",
            "optional": false,
            "field": "data.update_user",
            "description": "<p>修改用户，，具体字段见用户信息返回结果</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.create_time",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.update_time",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "    {\n        \"code\": 0,\n        \"msg\": \"操作成功\",\n        \"data\": [\n        {\n        \"id\": 2,\n        \"create_time\": \"2021-08-19 20:01:19\",\n        \"update_time\": \"2021-09-01 16:11:45\",\n        \"code\": \"token = \\\"123566788\\\"\",\n        \"create_user\": {\n        \"id\": 1,\n        \"create_time\": \"2021-08-18 14:01:49\",\n        \"update_time\": \"2021-08-20 23:42:46\",\n        \"phone\": \"18612532945\",\n        \"email\": \"admin@qq.com\",\n        \"password\": \"7da387a4cf65ab80356f8e4e38f1c290\",\n        \"nick\": \"水壶\",\n        \"avatar\": \"static/avatar/default.jpg\",\n        \"roles\": [\n        {\n        \"id\": 3,\n        \"create_time\": \"2021-08-20 23:40:55\",\n        \"update_time\": \"2021-08-20 23:40:55\",\n        \"name\": \"管理员\",\n        \"alias\": \"admin\"\n        }\n        ],\n        \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/default.jpg\"\n        },\n        \"update_user\": {\n        \"id\": 20,\n        \"create_time\": \"2021-09-01 15:28:46\",\n        \"update_time\": \"2021-09-01 15:28:46\",\n        \"phone\": \"13462245021\",\n        \"email\": \"763874651@qq.com\",\n        \"password\": \"b624e6b83fb16c6b0daf493af64e389f\",\n        \"nick\": \"tony\",\n        \"avatar\": \"static/avatar/default.jpg\",\n        \"roles\": [],\n        \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/default.jpg\"\n        }\n        }\n        ],\n        \"count\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "GetApiChameleonPublic_param",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/public_param"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/chameleon/interface",
    "title": "8、新增接口",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>接口名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "desc",
            "description": "<p>接口描述</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>接口归属分类id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>接口url</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "method",
            "description": "<p>接口请求方式id 0 get，1 post，2 put ，3 delete</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "response",
            "description": "<p>接口返回结果</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "response_header",
            "defaultValue": "{}",
            "description": "<p>接口响应头</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "code",
            "description": "<p>接口自定义代码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "proxy_url",
            "description": "<p>转发url,转发开关为开时必填</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "proxy_tag",
            "defaultValue": "false",
            "description": "<p>是否转发开关</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>接口状态</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "create_user",
            "description": "<p>创建用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "update_user",
            "description": "<p>修改用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "PostApiChameleonInterface",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/chameleon/interface_batch_delete",
    "title": "11、批量删除接口",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "List",
            "optional": false,
            "field": "ids",
            "description": "<p>要删除的id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "PostApiChameleonInterface_batch_delete",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface_batch_delete"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/chameleon/interface_category",
    "title": "4、新增接口分类",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>分类名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "prefix",
            "description": "<p>分类前缀</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create_user",
            "description": "<p>创建用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "update_user",
            "description": "<p>修改用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "PostApiChameleonInterface_category",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface_category"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/chameleon/public_param",
    "title": "2、提交公共代码",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>id，修改时必传</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>自定义代码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create_user",
            "description": "<p>创建用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "update_user",
            "description": "<p>修改用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "PostApiChameleonPublic_param",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/public_param"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/api/chameleon/interface",
    "title": "9、修改接口",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>接口名称</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>接口id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "desc",
            "description": "<p>接口描述</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>接口归属分类id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>接口url</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "method",
            "description": "<p>接口请求方式id 0 get，1 post，2 put ，3 delete</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "response",
            "description": "<p>接口返回结果</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "response_header",
            "defaultValue": "{}",
            "description": "<p>接口响应头</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "code",
            "description": "<p>接口自定义代码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "proxy_url",
            "description": "<p>转发url,转发开关为开时必填</p>"
          },
          {
            "group": "Parameter",
            "type": "Bool",
            "optional": false,
            "field": "proxy_tag",
            "defaultValue": "false",
            "description": "<p>是否转发开关</p>"
          },
          {
            "group": "Parameter",
            "type": "Bool",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>接口状态</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "create_user",
            "description": "<p>创建用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "update_user",
            "description": "<p>修改用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "PutApiChameleonInterface",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/api/chameleon/interface_category",
    "title": "5、修改接口分类",
    "group": "变色龙",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id，修改时必传</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>分类名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "prefix",
            "description": "<p>分类前缀</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create_user",
            "description": "<p>创建用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "update_user",
            "description": "<p>修改用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/chameleon/views.py",
    "groupTitle": "变色龙",
    "name": "PutApiChameleonInterface_category",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/chameleon/interface_category"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/qa_tools/interface_mock",
    "title": "4、mock接口数据构造",
    "group": "测试工具",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>生成数量</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name_prefix",
            "description": "<p>接口名称前缀</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url_prefix",
            "description": "<p>接口url前缀</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n     \"code\": 0,\n     \"msg\": \"操作成功\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/qa_tools/views.py",
    "groupTitle": "测试工具",
    "name": "GetApiQa_toolsInterface_mock",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/qa_tools/interface_mock"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/qa_tools/kabin_info",
    "title": "5、获取kabin信息",
    "group": "测试工具",
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "List",
            "optional": false,
            "field": "data",
            "description": "<p>卡bin信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n     \"code\": 0,\n     \"msg\": \"操作成功\",\n     \"data\":[]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/qa_tools/views.py",
    "groupTitle": "测试工具",
    "name": "GetApiQa_toolsKabin_info",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/qa_tools/kabin_info"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/qa_tools/person_info",
    "title": "3、四要素生成",
    "group": "测试工具",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "bank_code",
            "defaultValue": "ICBC",
            "description": "<p>银行编码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "card_type",
            "defaultValue": "DC",
            "description": "<p>银行卡类型 DC借记卡，CC信用卡</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.ssn",
            "description": "<p>身份证号</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>姓名</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.bank_card",
            "description": "<p>银行卡号</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n     \"code\": 0,\n     \"msg\": \"操作成功\",\n     \"data\": {\n         \"ssn\": \"140822195206196260\",\n         \"name\": \"刘玉英\",\n         \"phone\": \"18648817505\",\n         \"bank_card\": \"5240912931103001\"\n     }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/qa_tools/views.py",
    "groupTitle": "测试工具",
    "name": "GetApiQa_toolsPerson_info",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/qa_tools/person_info"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/qa_tools/create_sign",
    "title": "2、接口签名生成",
    "group": "测试工具",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>请求参数</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>接口签名</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\": 0,\n    \"msg\": \"操作成功\",\n    \"data\": \"269c2075b623e94629301c8cc81d4376\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/qa_tools/views.py",
    "groupTitle": "测试工具",
    "name": "PostApiQa_toolsCreate_sign",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/qa_tools/create_sign"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/qa_tools/encrypt_decrypt",
    "title": "1、接口加解密",
    "group": "测试工具",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>操作类型，1加密，2解密</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>加密/解密数据</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>加解密结果</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\": 0,\n    \"msg\": \"操作成功\",\n    \"data\": \"ekMAeJB6How1fWVs2eF2QQt1Qu3yVPQcUIWxbgsueFyNSVrCYJQymCjuGjoCNZmwuZLF2AlVTQJb0whMAw5hyg==\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/qa_tools/views.py",
    "groupTitle": "测试工具",
    "name": "PostApiQa_toolsEncrypt_decrypt",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/qa_tools/encrypt_decrypt"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/user/user_info",
    "title": "3、获取用户信息",
    "group": "用户",
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>数据返回</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>数据返回</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.nick",
            "description": "<p>昵称</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.full_avatar_url",
            "description": "<p>头像url</p>"
          },
          {
            "group": "返回参数",
            "type": "List",
            "optional": false,
            "field": "data.roles",
            "description": "<p>角色</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.update_time",
            "description": "<p>更新时间</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.create_time",
            "description": "<p>创建时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\": 0,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"id\": 2,\n        \"create_time\": \"2021-08-18 14:29:52\",\n        \"update_time\": \"2021-08-30 21:26:37\",\n        \"phone\": \"13810631245\",\n        \"email\": \"test1@qq.com\",\n        \"nick\": \"水果\",\n        \"roles\": [\"admin\"],\n        \"full_avatar_url\": \"http://sky.nnzhp.cn/static/avatar/sgbg_83hH1ex.jpeg\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/user/views.py",
    "groupTitle": "用户",
    "name": "GetApiUserUser_info",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/user/user_info"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/user/change_password",
    "title": "5、修改密码",
    "group": "用户",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "old_password",
            "description": "<p>旧密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_password",
            "description": "<p>新密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/user/views.py",
    "groupTitle": "用户",
    "name": "PostApiUserChange_password",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/user/change_password"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/user/login",
    "title": "2、登录",
    "group": "用户",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>手机号/邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>用户密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>sessionid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"注册成功！\",\n    \"token\":\"58bd8c4fe99ed66295b1c1628854864d\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/user/views.py",
    "groupTitle": "用户",
    "name": "PostApiUserLogin",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/user/login"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/logout",
    "title": "6、退出登录",
    "group": "用户",
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/user/views.py",
    "groupTitle": "用户",
    "name": "PostApiUserLogout",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/user/logout"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/user/register",
    "title": "1、注册",
    "group": "用户",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>用户手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>用户邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nick",
            "description": "<p>用户昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>用户密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password2",
            "description": "<p>密码确认</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": true,
            "field": "avatar",
            "description": "<p>头像</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>sessionid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"注册成功！\",\n    \"token\":\"58bd8c4fe99ed66295b1c1628854864d\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/user/views.py",
    "groupTitle": "用户",
    "name": "PostApiUserRegister",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/user/register"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/user_change",
    "title": "4、修改用户信息",
    "group": "用户",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>用户手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>用户邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nick",
            "description": "<p>用户昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": true,
            "field": "avatar",
            "description": "<p>头像</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/user/views.py",
    "groupTitle": "用户",
    "name": "PostApiUserUser_change",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/user/user_change"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/api/sparrow/student",
    "title": "1、获取学生列表",
    "group": "麻雀",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "20",
            "description": "<p>每页数量</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>第几页</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>模糊搜索</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sex",
            "description": "<p>性别</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "age",
            "description": "<p>年龄</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "grade",
            "description": "<p>班级</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "phone",
            "description": "<p>手机号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>总数量</p>"
          },
          {
            "group": "返回参数",
            "type": "List",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>学生id</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>学生名称</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.age",
            "description": "<p>年龄</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.addr",
            "description": "<p>地址</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.grade",
            "description": "<p>班级</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.phone",
            "description": "<p>手机号</p>"
          },
          {
            "group": "返回参数",
            "type": "Number",
            "optional": false,
            "field": "data.gold",
            "description": "<p>金币</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.create_time",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data.update_time",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n        \"code\": 0,\n        \"msg\": \"操作成功\",\n        \"data\": [\n            {\n            \"id\": 7,\n            \"create_time\": \"2021-08-25 20:18:44\",\n            \"update_time\": \"2021-08-25 20:18:44\",\n            \"name\": \"白菜\",\n            \"sex\": \"男\",\n            \"age\": 18,\n            \"addr\": \"北京市昌平区\",\n            \"grade\": \"天蝎座\",\n            \"phone\": \"12345678911\",\n            \"gold\": 100\n            }\n        ],\n        \"count\": 1\n        }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/sparrow/views.py",
    "groupTitle": "麻雀",
    "name": "GetApiSparrowStudent",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/sparrow/student"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/sparrow/student",
    "title": "2、新增学生",
    "group": "麻雀",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/json",
            "description": "<p>请求类型</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求头示例",
          "content": "\"Content-Type: application/json\"",
          "type": "Header"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grade",
            "description": "<p>班级</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "age",
            "defaultValue": "18",
            "description": "<p>年龄</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "addr",
            "defaultValue": "北京市昌平区",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "gold",
            "defaultValue": "100",
            "description": "<p>金币</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号，唯一，11位</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sex",
            "defaultValue": "男",
            "description": "<p>修改用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/sparrow/views.py",
    "groupTitle": "麻雀",
    "name": "PostApiSparrowStudent",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/sparrow/student"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/sparrow/student_encrypt",
    "title": "3、新增学生--加密",
    "group": "麻雀",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>请求参数加密后的密文，请求参数看接口2新增学生接口</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>返回结果，数据是加密的</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\",\n    \"data\":\"ekMAeJB6How1fWVs2eF2QQt1Qu3yVPQcUIWxbgsueFyNSVrCYJQymCjuGjoCNZmwuZLF2AlVTQJb0whMAw5hyg==\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/sparrow/views.py",
    "groupTitle": "麻雀",
    "name": "PostApiSparrowStudent_encrypt",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/sparrow/student_encrypt"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录后返回的token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "token示例",
          "content": "\"token: 58bd8c4fe99ed66295b1c1628854864d\"",
          "type": "Header"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/sparrow/student_sign",
    "title": "4、新增学生--需要签名",
    "group": "麻雀",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/json",
            "description": "<p>请求类型</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求头示例",
          "content": "\"Content-Type: application/json\"",
          "type": "Header"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grade",
            "description": "<p>班级</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "age",
            "defaultValue": "18",
            "description": "<p>年龄</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "addr",
            "defaultValue": "北京市昌平区",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "gold",
            "defaultValue": "100",
            "description": "<p>金币</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号，唯一，11位</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sex",
            "defaultValue": "男",
            "description": "<p>修改用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sign",
            "description": "<p>接口签名</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "返回参数": [
          {
            "group": "返回参数",
            "type": "int",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "返回参数",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>错误信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "返回报文",
          "content": "{\n    \"code\":0,\n    \"msg\":\"操作成功\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "starrysky_backend/sparrow/views.py",
    "groupTitle": "麻雀",
    "name": "PostApiSparrowStudent_sign",
    "sampleRequest": [
      {
        "url": "http://sky.nnzhp.cn/api/sparrow/student_sign"
      }
    ]
  }
] });
