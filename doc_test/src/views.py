
def register():
    """
    @api {post} /api/user/register 注册
    @apiGroup 用户
    @apiParam {String}  phone          用户手机号
    @apiParam {String}  email          用户邮箱
    @apiParam {String}  nick          用户昵称
    @apiParam {String}  password        用户密码
    @apiParam {String}  password2        密码确认
    @apiParam {File}  [avatar]        头像

    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {String} token  sessionid
    @apiSuccessExample {json} 返回报文
        {
            "code":0,
            "msg":"注册成功！",
            "token":"58bd8c4fe99ed66295b1c1628854864d"
        }

    """
    return 1




