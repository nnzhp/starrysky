import os, sys

base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.insert(0, base_dir)
from common.log import Log
from common.const import port


class RunServer:
    func_list = ["start", "restart", "stop"]

    @classmethod
    def start(cls):
        start_command = "cd %s && nohup daphne  starrysky_backend.asgi:application -p %s &" % (
            base_dir, port)
        Log.info("启动命令,{}", start_command)
        os.system(start_command)

    def stop(self):
        command = "ps -ef|grep  -v grep|grep daphne|grep  starrysky |awk '{print $2}'|xargs kill -9"
        Log.info("停止命令,{}", command)
        os.system(command)

    def restart(self):
        self.stop()
        self.start()

    @classmethod
    def run(cls):
        if len(sys.argv) > 1:
            command = sys.argv[1]
        else:
            command = "restart"
        obj = cls()
        if not hasattr(obj, command):
            Log.error("command error! stop/start/restart")
            raise Exception("command error! stop/start/restart")
        func = getattr(obj, command)
        func()


if __name__ == '__main__':
    RunServer.run()
