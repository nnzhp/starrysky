from django.contrib import admin

# Register your models here.

from . import models

admin.site.register(models.PublicParam)
admin.site.register(models.InterfaceCategory)
admin.site.register(models.Interface)