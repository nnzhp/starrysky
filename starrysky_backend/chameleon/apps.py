from django.apps import AppConfig


class ChameleonConfig(AppConfig):
    name = 'chameleon'
    verbose_name = "变色龙"

