from django.db import models
from common.custom_class import BaseModel
from user import models as user_models


# Create your models here.

class PublicParam(BaseModel):
    code = models.TextField(default='', verbose_name="自定义代码")
    create_user = models.ForeignKey(user_models.User, on_delete=models.SET(1), verbose_name="创建用户",
                                    related_name="c_user2",default=1)
    update_user = models.ForeignKey(user_models.User, on_delete=models.SET(1), verbose_name="修改用户",
                                    related_name="up_user2",default=1)

    def __str__(self):
        return "公共代码_%s" % self.id

    class Meta:
        db_table = "public_param"
        verbose_name = "公共参数"
        verbose_name_plural = verbose_name
        ordering = ["-update_time"]


class InterfaceCategory(BaseModel):
    name = models.CharField(verbose_name="分类名称", max_length=30, unique=True)
    prefix = models.CharField(verbose_name="分类前缀", max_length=40, unique=True)
    create_user = models.ForeignKey(user_models.User, on_delete=models.SET(1), verbose_name="创建用户",
                                    related_name="c_user1")
    update_user = models.ForeignKey(user_models.User, on_delete=models.SET(1), verbose_name="修改用户",
                                    related_name="up_user1")

    def __str__(self):
        return self.name

    class Meta:
        db_table = "interface_category"
        verbose_name = "接口分类"
        verbose_name_plural = verbose_name
        ordering = ["-update_time"]


class Interface(BaseModel):
    name = models.CharField(verbose_name="接口名称", max_length=30)
    desc = models.CharField(verbose_name="接口描述", max_length=100, default='', null=True, blank=True)
    url = models.CharField(verbose_name="接口url", max_length=50)
    category = models.ForeignKey(InterfaceCategory, verbose_name="接口归属分类", on_delete=models.CASCADE)
    method_choice = (
        (0, "get"),
        (1, "post"),
        (2, "put"),
        (3, "delete"),
    )
    method = models.SmallIntegerField(choices=method_choice, default=0, verbose_name="请求方式")
    response = models.TextField(verbose_name="返回结果", default='{}')
    response_header = models.TextField(verbose_name="响应头", default='{}', null=True, blank=True)
    code = models.TextField(verbose_name="自定义代码", null=True, blank=True)
    proxy_url = models.URLField(verbose_name="转发url", null=True, blank=True)
    proxy_tag = models.BooleanField(verbose_name="是否转发", default=False)
    status = models.BooleanField(verbose_name="接口状态", default=True)
    create_user = models.ForeignKey(user_models.User, on_delete=models.SET(1), verbose_name="创建用户",
                                    related_name="c_user")
    update_user = models.ForeignKey(user_models.User, on_delete=models.SET(1), verbose_name="修改用户",
                                    related_name="up_user")

    def __str__(self):
        return self.name

    class Meta:
        db_table = "interface"
        verbose_name = "接口"
        verbose_name_plural = verbose_name
        unique_together = ["url", 'category']  # 联合唯一
        ordering = ["-update_time"]
