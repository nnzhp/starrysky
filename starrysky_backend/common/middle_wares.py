from django.middleware.common import MiddlewareMixin
from django.http import QueryDict
import json
from .custom_response import FlyResponse
from .log import Log
import traceback
import copy

class PutMiddleWare(MiddlewareMixin):
    @staticmethod
    def process_request(request):
        if request.method.upper() == "PUT":
            content_type = request.META.get("CONTENT_TYPE", "")
            if "multipart" in content_type:  # form-data
                data, files = request.parse_file_upload(request.META, request)
                request.PUT = data
                request._files = files
            else:
                data = QueryDict(request.body)
                request.PUT = data


class JsonMiddleWare(MiddlewareMixin):

    @staticmethod
    def process_request(request):
        if request.method.upper() in ("PUT", "POST"):
            content_type = request.META.get("CONTENT_TYPE", "")
            if "application/json" in content_type:  # 入参是json
                try:
                    data = json.loads(request.body)
                    Log.info("request.body.json:{}", data)
                except:
                    return FlyResponse(-1, "json格式不合法")
                # if request.method.upper() == "POST":
                #     request.POST = data
                # else:
                #     request.PUT = data
                setattr(request, request.method.upper(), data)  # 反射


class ExceptionMiddleWare(MiddlewareMixin):

    # def process_request(self, request):
    #     req = copy.copy(request)
    #     Log.info("url_params:{}", req.GET)
    #     Log.info("form_data:{}", req.POST)
    #     Log.info("files:{}", req.FILES)
    #
    #
    # def process_response(self, request, response):
    #     Log.info("response:{}", response.content.decode())
    #     return response

    def process_exception(self, request, e):
        Log.error(traceback.format_exc())
        return FlyResponse(500, traceback.format_exc(), http_code=500)
