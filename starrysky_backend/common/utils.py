from itertools import chain
from django.core.files import File
from django.db.models.fields.related import ManyToManyField, ForeignKey
import datetime, hashlib
import pickle
import os
from common.custom_class import SkyDict
from common.log import Log
import zipfile

def model_to_dict(instance, fields=None, exclude=None):
    if instance == None: return None
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        value = f.value_from_object(instance)
        if type(value) == datetime.datetime:
            value = value.strftime("%Y-%m-%d %H:%M:%S")
        if type(value) == datetime.date:
            value = value.strftime("%Y-%m-%d")
        if isinstance(value, File):
            value = str(value)
        if isinstance(f, ManyToManyField):  # 处理多对多
            many_to_many_list = [model_to_dict(item) for item in value]
            value = many_to_many_list
        if isinstance(f, ForeignKey):  # 处理外键
            foreign_key_obj = getattr(instance, f.name)
            value = model_to_dict(foreign_key_obj)
        if f.choices:  # 处理枚举类型
            choices_dict = dict(f.choices)
            value = {"name": choices_dict.get(value), "value": value}
        data[f.name] = value
    for custom_field in opts._property_names:  # 处理自定义的字段
        if custom_field != "pk":
            data[custom_field] = getattr(instance, custom_field)
    return SkyDict(data)


class FormatError:

    @property
    def error_msg(self):
        result = ""
        for k, v in self.errors.get_json_data().items():
            message = v[0].get("message")
            msg = "%s%s" % (k, message)
            result += msg
        return result.replace("__all__", "")


def md5(s, salt="sfs@#21234"):
    s = str(s) + salt
    m = hashlib.md5(s.encode())
    return m.hexdigest()


class FileOperator:
    def __init__(self, name):
        self.name = name

    @Log.catch
    def pickle_read(self):
        with open(self.name, 'rb') as fr:
            return pickle.load(fr)

    @Log.catch
    def pickle_write(self, content):
        with open(self.name, 'wb') as fw:
            return pickle.dump(content, fw)


def is_chinese(s):
    """
    检查整个字符串是否包含中文
    :param string: 需要检查的字符串
    :return: bool
    """
    for ch in s:
        if u'\u4e00' <= ch <= u'\u9fff':
            return True

    return False


def write_file(file_name, content):
    with open(file_name, "w", encoding="utf-8") as fw:
        fw.write(content)


def remove_file(file_name):
    if os.path.exists(file_name):
        os.remove(file_name)


def zip_file(src_path,zip_file_name):
    zip_file_obj = zipfile.ZipFile(zip_file_name,"w")
    for cur_path,dirs,files in os.walk(src_path):
        if "__pycache__" in cur_path or "pytest_cache" in cur_path:
            continue
        for file in files:
            file_abs_path = os.path.join(cur_path,file)
            arcname =  file_abs_path.replace(os.path.dirname(src_path),".")
            zip_file_obj.write(file_abs_path,arcname)
    zip_file_obj.close()


def execute_sys_command(command):
    result = os.popen(command).read()
    return result