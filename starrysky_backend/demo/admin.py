from django.contrib import admin

# Register your models here.
from . import models



class BookAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'price', 'count','author','create_time'] #展示的字段
    list_filter = ['name'] #筛选
    search_fields = ['name'] #搜索


admin.site.register(models.Book,BookAdmin)
admin.site.register(models.Author)
admin.site.site_title = "sky"
admin.site.site_header = "sky后台管理"