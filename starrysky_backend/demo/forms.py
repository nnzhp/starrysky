from . import models
from common.custom_form import FlyForm, FlyCForm
from django import forms


class BookForm(FlyForm):
    class Meta:
        fields = '__all__'
        model = models.Book
        # exclude = [""]


class AuthorForm(FlyForm):
    class Meta:
        fields = '__all__'
        model = models.Author


class BookValueForm(FlyCForm):
    book_name = forms.CharField(max_length=30, min_length=2, required=False)
    author = forms.CharField(max_length=30, min_length=2, required=False)

    def clean_book_name(self):
        book_name = self.cleaned_data.get("book_name")
        if book_name:
            if book_name != 'all' and not models.Book.objects.filter(name=book_name).exists():
                self.add_error("book_name", "书名不存在！")
        return book_name

    def clean_author(self):
        author = self.cleaned_data.get("author")
        if author:
            if not models.Author.objects.filter(real_name=author).exists():
                self.add_error("author", "作者不存在！")
        return author

    def clean(self):
        book_name = self.cleaned_data.get("book_name")
        author = self.cleaned_data.get("author")

        if not book_name and not author:
            self.add_error("", "book_name/author必须填一个")

        return self.cleaned_data
