from django.shortcuts import render
from django.views import View
# Create your views here.
from common.custom_views import FlyView
from common.custom_response import FlyResponse
from common.utils import model_to_dict
from . import models,forms

class AuthorView(FlyView):
    form_class = forms.AuthorForm
    model_class = models.Author
    search_field = ["phone","real_name"]
    filter_field = ["phone"]

class BookView(FlyView):
    form_class = forms.BookForm
    model_class = models.Book

    def get(self, request): #这个例子是为了说明，如果需要新增字段的时候怎么操作
        if not self.check_page_param():
            return FlyResponse(-1, "分页信息不正确")
        data_list = []
        page_data, page_count = self.get_page_data()
        for row_obj in page_data: #表里面的每一行数据
            row_dict = model_to_dict(row_obj)
            row_dict["author_name"] = row_obj.author.real_name
            data_list.append(row_dict)
        return FlyResponse(data=data_list, count=page_count)

class BookValue(View):
    def get(self,request):
        form = forms.BookValueForm(request.GET)
        #book_name  all /xx
        #author
        if form.is_valid():
            book_name = form.cleaned_data["book_name"]
            author = form.cleaned_data["author"]
            price = 0
            if book_name:
                if book_name == "all":
                    all_book = models.Book.objects.all()
                    for book in all_book:
                        book_price = book.count * book.price
                        price += book_price
                else:
                    book = models.Book.objects.filter(name=book_name).first()
                    price = book.price * book.count
                return FlyResponse(money=price)

            if author:
                author = models.Author.objects.filter(real_name=author).first()
                for book in author.book_set.all():
                    book_price = book.count * book.price
                    price += book_price

            return FlyResponse(money=price)



        else:
            return FlyResponse(-1,form.error_msg)


