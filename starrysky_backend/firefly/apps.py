from django.apps import AppConfig


class FireflyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'firefly'
    verbose_name = "萤火虫"

