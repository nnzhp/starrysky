import os
from django.conf import settings

firefly_project_path = os.path.join(settings.BASE_DIR, "static", "firefly_projects")
firefly_project_report_path = os.path.join(settings.BASE_DIR, "static", "report")

INIT_PROJECT_UTIL_CODE = """
import jsonpath
import requests
import pymysql
import redis
import traceback
from rediscluster import RedisCluster
from config import Config
from hashlib import md5


class MySQL:
    def __init__(self, host, user, password, db, charset='utf8', autocommit=True, port=3306, cursor_type='dict'):
        self.port = int(port)
        self.host = host
        self.user = user
        self.password = password
        self.db = db
        self.charset = charset
        self.autocommit = autocommit
        self.cursor_type = cursor_type
        self.__connect()

    def __connect(self):
        print("【开始连接mysql】")
        print("【mysql配置】：", self.__dict__)
        try:
            self.conn = pymysql.connect(user=self.user,
                                        host=self.host, password=self.password, db=self.db,
                                        port=self.port,
                                        charset=self.charset, autocommit=self.autocommit)
            if self.cursor_type != 'dict':
                self.cursor = self.conn.cursor()
            else:
                self.cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        except:
            print('【mysql连接出错】：', traceback.format_exc())

    def __del__(self):
        if hasattr(self, "conn"):  # 如果有连接
            self.__close()

    def execute(self, sql):
        try:
            print("【开始执行sql】，sql：", sql)
            self.cursor.execute(sql)
        except:
            print('【sql执行出错】，sql语句是：%s,错误信息%s' % (sql, traceback.format_exc()))

    def fetchall(self, sql):
        self.execute(sql)
        data = self.cursor.fetchall()
        print("【sql执行结果】", data)
        return data

    def fetchone(self, sql):
        self.execute(sql)
        data = self.cursor.fetchone()
        print("【sql执行结果】", data)
        return data

    def __close(self):
        self.cursor.close()
        self.conn.close()


class DbCreator:

    @staticmethod
    def get_redis_single(env=None, config_var_name="redis_config", **kwargs):
        '''单机连接redis方法'''
        config_redis = getattr(Config.get_config(env), config_var_name)
        config_redis.update(kwargs)
        print("【redis单机配置信息】", config_redis)
        return redis.Redis(**config_redis)

    @staticmethod
    def get_redis_cluster(env=None, config_var_name="redis_cluster_config", **kwargs):
        '''集群连接redis方法'''
        config_redis = getattr(Config.get_config(env), config_var_name)
        config_redis.update(kwargs)
        print("【redis集群配置】", config_redis)
        return RedisCluster(**config_redis, skip_full_coverage_check=True)

    @staticmethod
    def get_mysql(env=None, config_var_name="mysql_config", **kwargs):
        mysql_config = getattr(Config.get_config(env), config_var_name)
        mysql_config.update(kwargs)
        return MySQL(**mysql_config)


def my_md5(s):
    s = str(s).encode()
    m = md5(s)
    return m.hexdigest()


class ObjDict(dict):
    '''字典转对象'''

    def __getattr__(self, item):  # {"login_time"}
        value = self.get(item)
        if type(value) == dict:
            value = ObjDict(value)  # 如果是字典类型，直接返回ObjDict这个类的对象

        elif isinstance(value, list) or isinstance(value, tuple):
            # 如果是list，循环list判断里面的元素，如果里面的元素是字典，那么就把字典转成ObjDict的对象
            value = list(value)
            for index, obj in enumerate(value):
                if isinstance(obj, dict):
                    value[index] = ObjDict(obj)
        return value


class Request:
    def __init__(self, url, params=None, data=None, headers=None, files=None, json=None, timeout=10):
        self.url = url
        self.params = params
        self.data = data
        self.headers = headers
        self.files = files
        self.json = json
        self.timeout = timeout

    def __get_response(self):
        try:
            ret = self.response.json()
        except:
            print("接口返回的不是json！无法转成字典！http_status:%s,response:%s" % (self.response.status_code,
                                                                     self.response.text))
            return ObjDict()
        print("【接口返回数据】", ret)
        return ObjDict(ret)

    def __request(self, method):
        print("【接口请求信息】url:%s,method:%s,params:%s,data:%s,json:%s,files:%s,headers:%s" % (
            self.url, method, self.params, self.data,
            self.json, self.files, self.headers))
        try:
            self.response = requests.request(method, url=self.url, params=self.params, data=self.data,
                                             json=self.json, headers=self.headers, files=self.files, verify=False,
                                             timeout=self.timeout)
        except:
            print("【接口不通】，请求出错,错误信息是:", traceback.format_exc())
            return ObjDict()

        return self.__get_response()

    def get(self):
        return self.__request("get")

    def post(self):
        return self.__request("post")

    def put(self):
        return self.__request("put")

    def delete(self):
        return self.__request("delete")


def get_dict_value(dic, key):
    ret = jsonpath.jsonpath(dic, "$..%s" % key)
    if ret:
        return ret if len(ret) > 1 else ret[0]

""".strip()
