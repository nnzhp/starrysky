import string
from django.core.exceptions import ValidationError
from common.custom_form import FlyForm, FlyCForm
from common.utils import is_chinese
from . import models
from django import forms


class ProjectForm(FlyForm):
    class Meta:
        fields = '__all__'
        model = models.Project

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors:
            if str(self.instance) == "":
                return cleaned_data
            if "name" not in self.changed_data:
                return cleaned_data
            # 如果是修改，判断有没有修改name，如果修改了，获取到old_name
            self.old_name = self.instance.name
            return cleaned_data


class FixtureForm(FlyForm):
    class Meta:
        fields = '__all__'
        model = models.Fixture


class PythonFileForm(FlyForm):
    def clean(self):
        cleaned_data = super().clean()
        if not self.errors:
            name = cleaned_data["name"].strip()
            if is_chinese(name):  # 如果名称里面含有中文
                raise ValidationError("名称必须是字母!")
            if not name.isidentifier():
                raise ValidationError("不是一个合法的名字!")
            if name == "config" or name == "conftest":
                raise ValidationError("不能是config/conftest")
            if name.startswith("test"):  # 防止和case文件同名
                raise ValidationError("不能以test开头")
            if str(self.instance) == "":  # 如果是新增 直接返回
                return cleaned_data
            if "name" not in self.changed_data:
                return cleaned_data
            # 如果是修改，判断有没有修改name，如果修改了，获取到old_name
            self.old_name = self.instance.name
            return cleaned_data

    class Meta:
        fields = '__all__'
        model = models.PythonFile


class CaseForm(FlyForm):
    class Meta:
        fields = '__all__'
        model = models.Case


class CaseRunForm(FlyCForm):
    id = forms.IntegerField(label="case_id")

    def clean_id(self):
        case_id = self.cleaned_data["id"]
        case = models.Case.objects.filter(id=case_id)
        if not case.exists():
            raise ValidationError(f"_{case_id}不存在！")
        if case.first().status == "运行中":
            raise ValidationError(f"_{case_id}正在运行中！")
        return case.first()


class CaseRunMoreForm(FlyCForm):
    project_id = forms.IntegerField()
    run_params = forms.CharField(required=False)
    report_desc = forms.CharField(required=False, empty_value="没有报告描述")

    def clean_project_id(self):
        project_id = self.cleaned_data["project_id"]
        project = models.Project.objects.filter(id=project_id)
        if not project.exists():
            raise ValidationError(f"_{project_id}不存在！")
        return project.first()
