# Generated by Django 3.2.6 on 2022-03-23 22:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firefly', '0011_auto_20220323_2203'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcaserunrecord',
            name='run_desc',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='运行描述'),
        ),
    ]
