# Generated by Django 3.2.6 on 2022-03-23 22:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firefly', '0012_projectcaserunrecord_run_desc'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectcaserunrecord',
            name='duration_time',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='运行时长'),
        ),
    ]
