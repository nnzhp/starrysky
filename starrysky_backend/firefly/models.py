from django.db import models
from common.custom_class import BaseModel
from user.models import User


class Project(BaseModel):
    name = models.CharField(verbose_name="项目名称", max_length=50, unique=True)
    code = models.TextField(verbose_name="代码")
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_cuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_muser")

    class Meta:
        verbose_name = "项目"
        verbose_name_plural = verbose_name
        ordering = ["-id", "update_time"]

    def __str__(self):
        return self.name


class PythonFile(BaseModel):
    name = models.CharField(verbose_name="文件名称", max_length=50)
    desc = models.CharField(verbose_name="文件说明", max_length=100)
    code = models.TextField(verbose_name="接口代码")
    project = models.ForeignKey(Project, on_delete=models.PROTECT, verbose_name="归属项目")
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_py_file_cuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_py_file_fmuser")

    class Meta:
        verbose_name = "接口"
        verbose_name_plural = verbose_name
        unique_together = ["name", "project"]
        ordering = ["-id", "update_time"]

    def __str__(self):
        return self.name


class Fixture(BaseModel):
    name = models.CharField(verbose_name="fixture名称", max_length=50)
    desc = models.CharField(verbose_name="fixture说明", max_length=100)
    code = models.TextField(verbose_name="fixture代码")
    type_choice = (
        (0, "接口"),
        (1, "fixture"),
    )
    project = models.OneToOneField(Project, on_delete=models.PROTECT, verbose_name="归属项目")
    # 1对1关系，一个项目只能有一个fixture文件
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_fixture_cuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_fixture_fmuser")

    class Meta:
        verbose_name = "fixture"
        verbose_name_plural = verbose_name
        unique_together = ["name", "project"]
        ordering = ["-id", "update_time"]

    def __str__(self):
        return self.name


class Case(BaseModel):
    name = models.CharField(verbose_name="用例名称", max_length=50)
    desc = models.CharField(verbose_name="用例描述", max_length=100)
    code = models.TextField(verbose_name="case代码")
    project = models.ForeignKey(Project, verbose_name="项目", on_delete=models.PROTECT, related_name="firefly_project")
    status = models.CharField(max_length=20, null=True, blank=True, verbose_name="用例状态")
    run_time = models.DateTimeField(verbose_name="运行时间", null=True, blank=True)
    duration_time = models.FloatField(verbose_name="运行时长", null=True, blank=True, default=0)
    run_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_case_run_user",
                                 verbose_name="运行用户", null=True, blank=True)
    run_result = models.TextField(null=True, verbose_name="运行结果", blank=True)
    create_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_case_cuser")
    update_user = models.ForeignKey(User, on_delete=models.SET(1), related_name="firefly_case_uuser")

    class Meta:
        verbose_name = "测试用例"
        verbose_name_plural = verbose_name
        unique_together = ["name", "project"]
        ordering = ["-id", "update_time"]

    def __str__(self):
        return self.name


class CaseRunRecord(BaseModel):
    case = models.ForeignKey(Case, on_delete=models.CASCADE, verbose_name="运行的case")
    run_code = models.TextField(verbose_name="运行代码")
    run_result = models.TextField(verbose_name="运行结果")
    run_user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="firefly_case_run_user_record",
                                 verbose_name="运行用户")
    status = models.CharField(max_length=20, verbose_name="用例状态")
    duration_time = models.FloatField(verbose_name="运行时长")

    class Meta:
        verbose_name = "case运行记录"
        verbose_name_plural = verbose_name
        ordering = ["-id"]


class ProjectCaseRunRecord(BaseModel):
    run_command = models.CharField(max_length=200, verbose_name="运行case参数", null=True, blank=True)
    run_desc = models.CharField(max_length=100, verbose_name="运行描述", null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name="归属项目",null=True,blank=True)
    duration_time = models.FloatField(verbose_name="运行时长",null=True,blank=True,default=0)
    run_user = models.ForeignKey(User, on_delete=models.DO_NOTHING, verbose_name="运行用户",null=True,blank=True)
    report_file_url = models.CharField(max_length=100, verbose_name="报告文件路径", null=True, blank=True)
    status = models.CharField(max_length=100, verbose_name="运行状态", null=True, blank=True)
    text_result = models.TextField(verbose_name="运行详细结果", null=True, blank=True)

    class Meta:
        verbose_name = "批量运行case记录"
        verbose_name_plural = verbose_name
        ordering = ["-id"]
