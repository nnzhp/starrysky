import os
from datetime import datetime

from common.log import Log
from firefly import models, const
from user import models as user_models
from common.utils import execute_sys_command


def format_case_run_result(run_result):
    '''格式case运行结果'''
    summary = run_result.splitlines()[-1]
    # 取最后一行的 ============================== 1 passed in 0.05s ===============================
    status, duration_time = summary.strip("=").strip().split(" in ")  # 去掉=去掉空格，按照 in 分割
    return status, duration_time.strip("s")


class CaseRun:
    def __init__(self, case, user):
        self.case = case
        self.user = user

    @property
    def case_file_path(self):
        project_case_path = os.path.join(const.firefly_project_path, self.case.project.name)
        case_file_path = os.path.join(project_case_path, f'test_{self.case.id}.py')
        return case_file_path

    def set_case_running(self):
        self.case.status = "运行中"
        self.case.run_user = self.user
        self.case.run_time = datetime.now()
        self.case.save()

    def run_case(self, case_file_path):
        command = rf"pytest -vs {case_file_path}"
        result = execute_sys_command(command)
        return result

    def save_record(self, run_result, status, duration_time):
        '''保存case运行记录'''

        # 更新case表中结果
        self.case.run_result = run_result
        self.case.duration_time = duration_time
        self.case.run_user = self.user  # 修改执行用户
        self.case.status = status  # 保存测试结果
        self.case.save()  # 保存到数据库

        # 每次运行都会插入一条记录
        case_run_record = models.CaseRunRecord()
        case_run_record.case = self.case
        case_run_record.run_result = run_result
        case_run_record.run_user = self.user
        case_run_record.run_code = self.case.code
        case_run_record.status = status
        case_run_record.duration_time = duration_time
        case_run_record.save()

    def run(self):
        self.set_case_running()
        run_result = self.run_case(self.case_file_path)
        status, duration_time = format_case_run_result(run_result)
        self.save_record(run_result, status, duration_time)  # 保存case运行结果


class CaseRunMore:
    def __init__(self, project: models.Project, user: user_models.User,
                 project_case_run_record: models.ProjectCaseRunRecord, run_params: str = "", report_desc: str = ""):
        self.project = project
        self.run_params = run_params
        self.user = user
        self.today = datetime.today()
        self.report_desc = report_desc
        self.project_case_run_record = project_case_run_record

    @property
    def report_filename(self):
        '''拼好html的文件名'''
        return f"report_{self.project.id}_{self.user.id}_{self.project_case_run_record.id}.html"

    @property
    def command(self):
        report_dir = self.get_report_dir()
        report_file_abs_path = os.path.join(report_dir, self.report_filename)  # 拼接html绝对路径
        report_title = f"{self.project.name}_{self.user.nick}_接口测试报告"  # html报告标题
        return rf"pytest {self.project_dir} -vs {self.run_params} --pytest_report {report_file_abs_path} " \
               rf"--pytest_title {report_title} --pytest_desc {self.report_desc} "

    def get_report_dir(self):
        report_dir = os.path.join(const.firefly_project_report_path, f'{self.today.year}', f'{self.today.month}',
                                  f'{self.today.day}')
        if not os.path.exists(report_dir):
            os.makedirs(report_dir)
        return report_dir

    @property
    def project_dir(self):
        return os.path.join(const.firefly_project_path, self.project.name)

    def set_running(self):
        self.project_case_run_record.run_user = self.user
        self.project_case_run_record.project = self.project
        self.project_case_run_record.status = "运行中"
        self.project_case_run_record.run_desc = self.report_desc
        self.project_case_run_record.save()

    @property
    def report_file_url(self):
        return f'/static/report/{self.today.year}/{self.today.month}/{self.today.day}/{self.report_filename}'

    def run(self):
        # 生成记录
        self.set_running()
        case_text_result = execute_sys_command(self.command)
        self.project_case_run_record.run_command = self.command
        Log.debug("执行case的命令：{}", self.command)
        status, duration_time = format_case_run_result(case_text_result)
        self.project_case_run_record.text_result = case_text_result
        self.project_case_run_record.status = status
        self.project_case_run_record.report_file_url = self.report_file_url
        self.project_case_run_record.duration_time = duration_time
        self.project_case_run_record.save()
        self.send()

    def send(self):
        # todo:待实现发送邮件
        pass
