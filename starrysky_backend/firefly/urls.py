from django.urls import path
from . import views

urlpatterns = [
    path('project', views.ProjectView.as_view()),
    path('python_file', views.PythonFileView.as_view()),
    path('fixture', views.FixtureView.as_view()),
    path('case', views.CaseView.as_view()),
    path('case_run', views.CaseRunView.as_view()),
    path('query_result', views.query_result),
    path('down_load_project', views.down_project_file),
    path('pip', views.pip_install),
    path('case_run_project', views.RunCaseProjectView.as_view()),
    path('query_report_detail', views.query_report_detail),

]
