import os
import shutil

from common.log import Log
from firefly import const
from django.views import View
from django.http.response import FileResponse
from common.custom_views import FlyView, BaseView, GetView
from common.utils import write_file, remove_file, zip_file, execute_sys_command
from common.custom_response import FlyResponse
from common.utils import model_to_dict
from . import forms, models
from firefly.service.case_run import CaseRun, CaseRunMore
import threading


class ProjectView(FlyView):
    search_field = ["name", "code"]
    filter_field = ["id"]
    form_class = forms.ProjectForm
    model_class = models.Project

    def post(self, request):
        ret = super(ProjectView, self).post(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.name)
            os.mkdir(project_dir_name)
            project_config_file = os.path.join(project_dir_name, "config.py")  # 写配置
            project_util_file = os.path.join(project_dir_name, "utils.py")  # 写公共方法
            write_file(project_util_file, const.INIT_PROJECT_UTIL_CODE)
            write_file(project_config_file, self.db_instance.code)
            util_obj = models.PythonFile(name="utils", desc=self.db_instance.name + "_公共方法",
                                         code=const.INIT_PROJECT_UTIL_CODE,
                                         project=self.db_instance,
                                         create_user=self.request.user,
                                         update_user=self.request.user,
                                         )
            util_obj.save()
        return ret

    def put(self, request):
        ret = super(ProjectView, self).put(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.name)
            if hasattr(self.form, "old_name"):  # 如果是修改了项目名称，就改一下文件夹的名字
                project_dir_name_old = os.path.join(const.firefly_project_path, self.form.old_name)
                os.rename(project_dir_name_old, project_dir_name)
            project_config_file = os.path.join(project_dir_name, "config.py")
            write_file(project_config_file, self.db_instance.code)
        return ret

    def delete(self, request):
        id = request.GET.get("id")
        if not id:
            return FlyResponse(-1, "请传入要删除的id")
        obj = self.model_class.objects.filter(id=id)
        if not obj.exists():
            return FlyResponse(-1, "删除的id不存在")
        try:
            obj.delete()
        except Exception as e:
            return FlyResponse(-1, msg="删除失败，请检查是否有关联的数据！%s" % e)
        else:
            project_dir_name = os.path.join(const.firefly_project_path, obj.first().name)
            if os.path.exists(project_dir_name):  # 如果文件夹存在才改名，不实际删除
                shutil.rmtree(project_dir_name)
        return FlyResponse()

    # get
    """
    @api {get} /api/chameleon/interface_category 3、获取接口分类列表
    @apiGroup 萤火虫
    @apiUse TokenHeader
    @apiParam {Number}  limit=20        每页数量
    @apiParam {Number}  page=1          第几页
    @apiParam {String}  [search]        模糊搜索
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {Number} count  总数量
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  分类id
    @apiSuccess (返回参数) {String} data.name  分类名称，唯一
    @apiSuccess (返回参数) {String} data.prefix  分类前缀，唯一
    @apiSuccess (返回参数) {json} data.create_user  创建用户，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {json} data.update_user  修改用户，，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间
    @apiSuccessExample {json} 返回报文
        {
	"code": 0,
	"msg": "操作成功",
	"data": [{
		"id": 5,
		"create_time": "2021-08-31 20:08:38",
		"update_time": "2021-08-31 20:08:38",
		"name": "订单中心",
		"prefix": "order",
		"create_user": {
			"id": 14,
			"create_time": "2021-08-27 22:20:27",
			"update_time": "2021-08-27 22:20:27",
			"phone": "13812531232",
			"email": "admin1@qq.com",
			"password": "7da387a4cf65ab80356f8e4e38f1c290",
			"nick": "你是猪",
			"avatar": "static/avatar/aaa.jpeg",
			"roles": [],
			"full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
		},
		"update_user": {
            "id": 14,
            "create_time": "2021-08-27 22:20:27",
            "update_time": "2021-08-27 22:20:27",
            "phone": "13812531232",
            "email": "admin1@qq.com",
            "password": "7da387a4cf65ab80356f8e4e38f1c290",
            "nick": "你是猪",
            "avatar": "static/avatar/aaa.jpeg",
            "roles": [],
            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
            }
	}],
	"count": 1
}

    """
    # post
    """
        @api {post} /api/chameleon/interface_category 4、新增接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # put
    """
        @api {put} /api/chameleon/interface_category 5、修改接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id，修改时必传
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # delete
    """
        @api {delete} /api/chameleon/interface_category 6、删除接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """


class FixtureView(FlyView):
    search_field = ["name", "code", "desc"]
    filter_field = ["id", "project"]
    form_class = forms.FixtureForm
    model_class = models.Fixture

    def post(self, request):
        ret = super(FixtureView, self).post(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
            fixture_file = os.path.join(project_dir_name, "conftest.py")
            write_file(fixture_file, self.db_instance.code)
        return ret

    def put(self, request):
        ret = super(FixtureView, self).put(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
            fixture_file = os.path.join(project_dir_name, "conftest.py")
            write_file(fixture_file, self.db_instance.code)
        return ret

    def delete(self, request):
        id = request.GET.get("id")
        if not id:
            return FlyResponse(-1, "请传入要删除的id")
        obj = self.model_class.objects.filter(id=id)
        if not obj.exists():
            return FlyResponse(-1, "删除的id不存在")
        self.db_instance = obj.first()
        project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
        fixture_file = os.path.join(project_dir_name, "conftest.py")
        remove_file(fixture_file)
        obj.delete()
        return FlyResponse()

    # get
    """
    @api {get} /api/chameleon/interface_category 3、获取接口分类列表
    @apiGroup 萤火虫
    @apiUse TokenHeader
    @apiParam {Number}  limit=20        每页数量
    @apiParam {Number}  page=1          第几页
    @apiParam {String}  [search]        模糊搜索
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {Number} count  总数量
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  分类id
    @apiSuccess (返回参数) {String} data.name  分类名称，唯一
    @apiSuccess (返回参数) {String} data.prefix  分类前缀，唯一
    @apiSuccess (返回参数) {json} data.create_user  创建用户，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {json} data.update_user  修改用户，，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间
    @apiSuccessExample {json} 返回报文
        {
	"code": 0,
	"msg": "操作成功",
	"data": [{
		"id": 5,
		"create_time": "2021-08-31 20:08:38",
		"update_time": "2021-08-31 20:08:38",
		"name": "订单中心",
		"prefix": "order",
		"create_user": {
			"id": 14,
			"create_time": "2021-08-27 22:20:27",
			"update_time": "2021-08-27 22:20:27",
			"phone": "13812531232",
			"email": "admin1@qq.com",
			"password": "7da387a4cf65ab80356f8e4e38f1c290",
			"nick": "你是猪",
			"avatar": "static/avatar/aaa.jpeg",
			"roles": [],
			"full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
		},
		"update_user": {
            "id": 14,
            "create_time": "2021-08-27 22:20:27",
            "update_time": "2021-08-27 22:20:27",
            "phone": "13812531232",
            "email": "admin1@qq.com",
            "password": "7da387a4cf65ab80356f8e4e38f1c290",
            "nick": "你是猪",
            "avatar": "static/avatar/aaa.jpeg",
            "roles": [],
            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
            }
	}],
	"count": 1
}

    """
    # post
    """
        @api {post} /api/chameleon/interface_category 4、新增接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # put
    """
        @api {put} /api/chameleon/interface_category 5、修改接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id，修改时必传
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # delete
    """
        @api {delete} /api/chameleon/interface_category 6、删除接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """


class PythonFileView(FlyView):
    search_field = ["name", "desc", "code"]
    filter_field = ["project", "id"]
    form_class = forms.PythonFileForm
    model_class = models.PythonFile

    # get
    """
    @api {get} /api/chameleon/interface_category 3、获取接口分类列表
    @apiGroup 萤火虫
    @apiUse TokenHeader
    @apiParam {Number}  limit=20        每页数量
    @apiParam {Number}  page=1          第几页
    @apiParam {String}  [search]        模糊搜索
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {Number} count  总数量
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  分类id
    @apiSuccess (返回参数) {String} data.name  分类名称，唯一
    @apiSuccess (返回参数) {String} data.prefix  分类前缀，唯一
    @apiSuccess (返回参数) {json} data.create_user  创建用户，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {json} data.update_user  修改用户，，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间
    @apiSuccessExample {json} 返回报文
        {
	"code": 0,
	"msg": "操作成功",
	"data": [{
		"id": 5,
		"create_time": "2021-08-31 20:08:38",
		"update_time": "2021-08-31 20:08:38",
		"name": "订单中心",
		"prefix": "order",
		"create_user": {
			"id": 14,
			"create_time": "2021-08-27 22:20:27",
			"update_time": "2021-08-27 22:20:27",
			"phone": "13812531232",
			"email": "admin1@qq.com",
			"password": "7da387a4cf65ab80356f8e4e38f1c290",
			"nick": "你是猪",
			"avatar": "static/avatar/aaa.jpeg",
			"roles": [],
			"full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
		},
		"update_user": {
            "id": 14,
            "create_time": "2021-08-27 22:20:27",
            "update_time": "2021-08-27 22:20:27",
            "phone": "13812531232",
            "email": "admin1@qq.com",
            "password": "7da387a4cf65ab80356f8e4e38f1c290",
            "nick": "你是猪",
            "avatar": "static/avatar/aaa.jpeg",
            "roles": [],
            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
            }
	}],
	"count": 1
}

    """
    # post
    """
        @api {post} /api/chameleon/interface_category 4、新增接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # put
    """
        @api {put} /api/chameleon/interface_category 5、修改接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id，修改时必传
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # delete
    """
        @api {delete} /api/chameleon/interface_category 6、删除接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """

    def post(self, request):
        ret = super(PythonFileView, self).post(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
            py_file = os.path.join(project_dir_name, f"{self.db_instance.name}.py")
            write_file(py_file, self.db_instance.code)
        return ret

    def put(self, request):
        ret = super(PythonFileView, self).put(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
            py_file = os.path.join(project_dir_name, f"{self.db_instance.name}.py")
            if hasattr(self.form, "old_name"):
                py_file_old = os.path.join(project_dir_name, f"{self.form.old_name}.py")
                os.rename(py_file_old, py_file)
            write_file(py_file, self.db_instance.code)
        return ret

    def delete(self, request):
        id = request.GET.get("id")
        if not id:
            return FlyResponse(-1, "请传入要删除的id")
        obj = self.model_class.objects.filter(id=id)
        if not obj.exists():
            return FlyResponse(-1, "删除的id不存在")
        py_file_path = os.path.join(const.firefly_project_path, obj.first().project.name, f"{obj.first().name}.py")
        remove_file(py_file_path)
        obj.delete()
        return FlyResponse()


class CaseView(FlyView):
    search_field = ["name", "desc", "code"]
    filter_field = ["project", "id"]
    form_class = forms.CaseForm
    model_class = models.Case

    def post(self, request):
        ret = super(CaseView, self).post(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
            case_file = os.path.join(project_dir_name, f"test_{self.db_instance.id}.py")
            write_file(case_file, self.db_instance.code)
        return ret

    def put(self, request):
        ret = super(CaseView, self).put(request)
        if ret.code == 0:
            project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
            case_file = os.path.join(project_dir_name, f"test_{self.db_instance.id}.py")
            write_file(case_file, self.db_instance.code)
        return ret

    def delete(self, request):
        id = request.GET.get("id")
        if not id:
            return FlyResponse(-1, "请传入要删除的id")
        obj = self.model_class.objects.filter(id=id)
        if not obj.exists():
            return FlyResponse(-1, "删除的id不存在")
        self.db_instance = obj.first()
        project_dir_name = os.path.join(const.firefly_project_path, self.db_instance.project.name)
        case_file = os.path.join(project_dir_name, f"test_{self.db_instance.id}.py")
        remove_file(case_file)
        self.db_instance.delete()
        return FlyResponse()

    # get
    """
    @api {get} /api/chameleon/interface_category 3、获取接口分类列表
    @apiGroup 萤火虫
    @apiUse TokenHeader
    @apiParam {Number}  limit=20        每页数量
    @apiParam {Number}  page=1          第几页
    @apiParam {String}  [search]        模糊搜索
    @apiSuccess (返回参数) {int} code  状态码
    @apiSuccess (返回参数) {String} msg  错误信息
    @apiSuccess (返回参数) {Number} count  总数量
    @apiSuccess (返回参数) {List} data  返回数据
    @apiSuccess (返回参数) {Number} data.id  分类id
    @apiSuccess (返回参数) {String} data.name  分类名称，唯一
    @apiSuccess (返回参数) {String} data.prefix  分类前缀，唯一
    @apiSuccess (返回参数) {json} data.create_user  创建用户，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {json} data.update_user  修改用户，，具体字段见用户信息返回结果
    @apiSuccess (返回参数) {String} data.create_time  创建时间
    @apiSuccess (返回参数) {String} data.update_time  修改时间
    @apiSuccessExample {json} 返回报文
        {
	"code": 0,
	"msg": "操作成功",
	"data": [{
		"id": 5,
		"create_time": "2021-08-31 20:08:38",
		"update_time": "2021-08-31 20:08:38",
		"name": "订单中心",
		"prefix": "order",
		"create_user": {
			"id": 14,
			"create_time": "2021-08-27 22:20:27",
			"update_time": "2021-08-27 22:20:27",
			"phone": "13812531232",
			"email": "admin1@qq.com",
			"password": "7da387a4cf65ab80356f8e4e38f1c290",
			"nick": "你是猪",
			"avatar": "static/avatar/aaa.jpeg",
			"roles": [],
			"full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
		},
		"update_user": {
            "id": 14,
            "create_time": "2021-08-27 22:20:27",
            "update_time": "2021-08-27 22:20:27",
            "phone": "13812531232",
            "email": "admin1@qq.com",
            "password": "7da387a4cf65ab80356f8e4e38f1c290",
            "nick": "你是猪",
            "avatar": "static/avatar/aaa.jpeg",
            "roles": [],
            "full_avatar_url": "http://sky.nnzhp.cn/static/avatar/aaa.jpeg"
            }
	}],
	"count": 1
}

    """
    # post
    """
        @api {post} /api/chameleon/interface_category 4、新增接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # put
    """
        @api {put} /api/chameleon/interface_category 5、修改接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id，修改时必传
        @apiParam {String}  name          分类名称
        @apiParam {String}  prefix         分类前缀
        @apiParam {String}  create_user    创建用户id
        @apiParam {String}  update_user     修改用户id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """
    # delete
    """
        @api {delete} /api/chameleon/interface_category 6、删除接口分类
        @apiGroup 变色龙
        @apiUse TokenHeader
        @apiParam {Number}  id          id
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccessExample {json} 返回报文
            {
                "code":0,
                "msg":"操作成功"
            }
        """


class CaseRunView(View):
    def get(self, request):
        form = forms.CaseRunForm(request.GET)
        if form.is_valid():
            case = form.cleaned_data["id"]  # 这里不取id是因为，form里面查过了，返回的数据加工成case对象了
            case_run_obj = CaseRun(case, request.user)
            threading.Thread(target=case_run_obj.run).start()  # 启动线程运行
            return FlyResponse(data=case.id)
        return FlyResponse(-1, msg=form.error_msg)


def query_result(request):
    case_id = request.GET.get("id")
    case = models.Case.objects.get(id=case_id)
    if case.status != "运行中":
        return FlyResponse(stutus=0, data=model_to_dict(case))
    return FlyResponse(status=1, data=model_to_dict(case))


class CaseRunRecordView(BaseView, GetView):
    model_class = models.CaseRunRecord


def down_project_file(request):
    project_id = request.GET.get("id")
    project = models.Project.objects.filter(id=project_id)
    if not project.exists():
        return FlyResponse(-1, msg="项目不存在！")
    project_path = os.path.join(const.firefly_project_path, project.first().name)

    zip_file_name = os.path.join(const.firefly_project_path, f"{project.first().name}.zip")
    zip_file(project_path, zip_file_name)
    fr = open(zip_file_name, 'rb')
    return FileResponse(fr)


def pip_install(request):
    model_name = request.GET.get("model_name")
    if not model_name:
        return FlyResponse(-1, "模块名称不能为空")
    if request.method == "GET":
        command = f"/usr/anaconda3/bin/pip list|grep -i '{model_name}' "
    else:
        command = f"/usr/anaconda3/bin/pip install {model_name} --user"
    result = execute_sys_command(command)
    Log.debug("pip command result : {}", result)
    return FlyResponse(data=result)


class RunCaseProjectView(BaseView, GetView):
    model_class = models.ProjectCaseRunRecord
    exclude_field = ["text_result"]
    search_field = ["run_command", "text_result", "status"]
    filter_field = ["project"]

    def post(self, request):
        form = forms.CaseRunMoreForm(request.POST)
        if form.is_valid():
            project = form.cleaned_data["project_id"]
            run_params = form.cleaned_data["run_params"]
            report_desc = form.cleaned_data["report_desc"]
            project_case_run_record_obj = models.ProjectCaseRunRecord()
            project_case_run_record_obj.save()
            # 这里直接实例化是因为要返回记录的id，如果这里不直接保存的话，获取不到那一条记录的id
            case_run_more = CaseRunMore(project, request.user, project_case_run_record_obj, run_params, report_desc)
            threading.Thread(target=case_run_more.run).start()
            return FlyResponse(data=project_case_run_record_obj.id)

        return FlyResponse(-1, form.error_msg)


def query_report_detail(request):
    report_id = request.GET.get("id", 0)
    case_run_record = models.ProjectCaseRunRecord.objects.filter(id=report_id)
    if not case_run_record.exists():
        return FlyResponse(-1, "case运行记录不存在")
    if case_run_record.first().status != "运行中":
        return FlyResponse(stutus=0, data=model_to_dict(case_run_record.first()))
    return FlyResponse(status=1, data=model_to_dict(case_run_record.first()))
