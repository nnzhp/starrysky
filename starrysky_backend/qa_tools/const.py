import os

cur_app_path = os.path.dirname(os.path.abspath(__file__))

public_key = '''
-----BEGIN RSA PUBLIC KEY-----
MEgCQQCMDxzHhqIEs8sr3/vYwwMXUlgOQ48ifTt9xe8fmAJ+25zfUMHeCjCt/X9O
PkG64Us4i5iDIPPavOxvZ3JoRMTDAgMBAAE=
-----END RSA PUBLIC KEY-----
'''
private_key='''
-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBAIwPHMeGogSzyyvf+9jDAxdSWA5DjyJ9O33F7x+YAn7bnN9Qwd4K
MK39f04+QbrhSziLmIMg89q87G9ncmhExMMCAwEAAQJAFLtj8jD+qJ2IlI0KNZLg
UUJyIGJlvKA2/y4ecFnK3SPvfK34ajH0ElNQlhcjL5zZjxvYiAHPRx2Zv8MRYWG6
SQIjANW1n1WH1cwJtWnhrcPK7n0y9WAET1/vlhjyf2FJQIUoUg8CHwCnxminW9RQ
hQ4B0ZJji1Zkub5MbxH2vJvHc0svxg0CImZRM3zOsQSkLVAJNZmFAhO9Hw+eYKZM
3e8JwwS//n0CJe8CHmavMbhUpNKPTO5iBRW13y+Q217OkBESiMl+0CKyNQIiWdFs
NXlPZcH0JXR6gLtGidxK1vVM3d1tE5v4LQUtRXp9Cw==
-----END RSA PRIVATE KEY-----
'''

kabin_json_file = os.path.join(cur_app_path,"data","kabin.json")