import json


def get_bank_names():
    '''从文件中读取银行名称和编码'''
    with open("name.csv") as f2:
        names_dic = {}
        for index, line in enumerate(f2):
            if index == 0:
                continue
            line = line.strip()
            if line:
                code, name = line.split(',')
                names_dic[code] = name
        return names_dic


def generate_kabin():
    bins = {}

    bank_names = get_bank_names()
    f1 = open("bin.csv")

    for index, line in enumerate(f1):
        if index == 0:
            continue
        line = line.strip()
        if not line:
            continue

        prefix, code, type, length = line.split(',')
        bank_name = bank_names.get(code)
        if not bank_name:
            continue
        bin_dic = {"prefix": prefix, "length": int(length)}
        if code not in bins:
            bins[code] = {
                type: [bin_dic],
                "name": bank_name
            }
        else:
            if bins[code].get(type):
                bins[code][type].append(bin_dic)
            else:
                bins[code][type] = [bin_dic]

    f1.close()
    return bins


def write_to_json():
    kabin = generate_kabin()
    with open("kabin.json", "w", encoding="utf-8") as fw:
        json.dump(kabin, fw, ensure_ascii=False, indent=4)

if __name__ == '__main__':
    write_to_json()