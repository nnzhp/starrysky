import faker, json, random
from qa_tools import const
import string

f = faker.Faker(locale="zh-CN")


class BankCard:

    def __init__(self, bank_code, card_type="DC"):
        self.bank_code = bank_code
        self.card_type = card_type.upper()
        self.kabin_info = self.get_kabin_info()


    @staticmethod
    def get_kabin_info():
        with open(const.kabin_json_file, encoding="utf-8") as fr:
            return json.load(fr)

    def check_bank_param(self):
        bank_info = self.kabin_info.get(self.bank_code)
        if not bank_info:
            raise Exception("银行不存在")
        card_type_info = bank_info.get(self.card_type)
        if not card_type_info:
            raise Exception("该银行下没有%s类型的银行卡" % self.bank_code)

    def check_bank_card_isvalid(self, card_num):
        s = 0
        card_num_length = len(card_num)
        for _ in range(1, card_num_length + 1):
            t = int(card_num[card_num_length - _])
            if _ % 2 == 0:
                t *= 2
                s += t if t < 10 else t % 10 + t // 10
            else:
                s += t
        return s % 10 == 0

    def get_bank_card_no(self):
        self.check_bank_param()
        while True:
            card_info = random.choice(self.kabin_info.get(self.bank_code).get(self.card_type))
            length = card_info.get("length")
            prefix = card_info.get("prefix")
            other_length = length - len(prefix)
            other_number = ''.join([random.choice(string.digits) for _ in range(other_length)])
            bank_card_no = prefix + other_number
            if self.check_bank_card_isvalid(bank_card_no):
                return bank_card_no


def get_person(bank_code, card_type="DC"):
    ssn = f.ssn()
    name = f.name()
    phone = f.phone_number()
    card_no = BankCard(bank_code, card_type)
    return {"ssn":ssn,"name":name,"phone":phone,"bank_card":card_no.get_bank_card_no()}



if __name__ == '__main__':
    b = BankCard("ICBC", "CC")
    ret = b.get_bank_card_no()
    ret2 = get_person("ICBC", "CC")
    print(ret)
    print(ret2)
