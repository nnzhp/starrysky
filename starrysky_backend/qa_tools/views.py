from common.custom_response import FlyResponse
from django.views import View
from . import forms
from qa_tools.services import data_encrypt_decrypt, data_sign, person_bank,create_mock_interface
from . import const
import json


# Create your views here.

class InterfaceEncryptDecrypt(View):

    def post(self, request):
        # post
        """
            @api {post} /api/qa_tools/encrypt_decrypt 1、接口加解密
            @apiGroup 测试工具
            @apiUse TokenHeader
            @apiParam {String}  type          操作类型，1加密，2解密
            @apiParam {String}  data         加密/解密数据
            @apiSuccess (返回参数) {int} code  状态码
            @apiSuccess (返回参数) {String} msg  错误信息
            @apiSuccess (返回参数) {String} data  加解密结果
            @apiSuccessExample {json} 返回报文
                {
                    "code": 0,
                    "msg": "操作成功",
                    "data": "ekMAeJB6How1fWVs2eF2QQt1Qu3yVPQcUIWxbgsueFyNSVrCYJQymCjuGjoCNZmwuZLF2AlVTQJb0whMAw5hyg=="
                }
            """
        form = forms.InterfaceEncryptDecryptForm(request.POST)
        if form.is_valid():
            type = form.cleaned_data.get("type")
            data = form.cleaned_data.get("data")
            result = data_encrypt_decrypt.encrypt_decrypt(type, data)
            return FlyResponse(data=result)
        return FlyResponse(-1, msg=form.error_msg)


class CreateSignView(View):

    def post(self, request):
        # post
        """
            @api {post} /api/qa_tools/create_sign 2、接口签名生成
            @apiGroup 测试工具
            @apiUse TokenHeader
            @apiParam {Json}  data         请求参数
            @apiSuccess (返回参数) {int} code  状态码
            @apiSuccess (返回参数) {String} msg  错误信息
            @apiSuccess (返回参数) {String} data  接口签名
            @apiSuccessExample {json} 返回报文
                {
                    "code": 0,
                    "msg": "操作成功",
                    "data": "269c2075b623e94629301c8cc81d4376"
                }
            """
        form = forms.CreateSignForm(request.POST)
        if form.is_valid():
            sign_data = form.cleaned_data.get("data")
            ret = data_sign.create_sign(sign_data)
            return FlyResponse(data=ret)
        return FlyResponse(-1, msg=form.error_msg)


class PersonView(View):
    def get(self, request):
        """
            @api {get} /api/qa_tools/person_info 3、四要素生成
            @apiGroup 测试工具
            @apiUse TokenHeader
            @apiParam {String}  bank_code=ICBC         银行编码
            @apiParam {String}  card_type=DC         银行卡类型 DC借记卡，CC信用卡
            @apiSuccess (返回参数) {int} code  状态码
            @apiSuccess (返回参数) {String} msg  错误信息
            @apiSuccess (返回参数) {Json} data  返回数据
            @apiSuccess (返回参数) {String} data.ssn  身份证号
            @apiSuccess (返回参数) {String} data.name  姓名
            @apiSuccess (返回参数) {String} data.phone  手机号
            @apiSuccess (返回参数) {String} data.bank_card  银行卡号
            @apiSuccessExample {json} 返回报文
               {
                    "code": 0,
                    "msg": "操作成功",
                    "data": {
                        "ssn": "140822195206196260",
                        "name": "刘玉英",
                        "phone": "18648817505",
                        "bank_card": "5240912931103001"
                    }
                }
            """
        form = forms.PersonForm(request.GET)
        if form.is_valid():
            bank_code = form.cleaned_data["bank_code"]
            card_type = form.cleaned_data["card_type"]
            ret = person_bank.get_person(bank_code, card_type)
            return FlyResponse(data=ret)
        return FlyResponse(-1, msg=form.error_msg)


class InterfaceMockView(View):
    def get(self, request):
        """
            @api {get} /api/qa_tools/interface_mock 4、mock接口数据构造
            @apiGroup 测试工具
            @apiUse TokenHeader
            @apiParam {Number}  limit         生成数量
            @apiParam {String}  name_prefix   接口名称前缀
            @apiParam {String}  url_prefix    接口url前缀
            @apiSuccess (返回参数) {int} code  状态码
            @apiSuccess (返回参数) {String} msg  错误信息

            @apiSuccessExample {json} 返回报文
               {
                    "code": 0,
                    "msg": "操作成功"
                }
            """
        form = forms.InterfaceMockForm(request.GET)
        if form.is_valid():
            limit = form.cleaned_data["limit"]
            url_prefix = form.cleaned_data["url_prefix"]
            name_prefix = form.cleaned_data["name_prefix"]
            create_mock_interface.create_interface(name_prefix,url_prefix,limit)
            return FlyResponse()
        return FlyResponse(-1, msg=form.error_msg)


def get_kabin(request):
    """
        @api {get} /api/qa_tools/kabin_info 5、获取kabin信息
        @apiGroup 测试工具
        @apiSuccess (返回参数) {int} code  状态码
        @apiSuccess (返回参数) {String} msg  错误信息
        @apiSuccess (返回参数) {List} data  卡bin信息

        @apiSuccessExample {json} 返回报文
           {
                "code": 0,
                "msg": "操作成功",
                "data":[]
            }
        """
    with open(const.kabin_json_file,encoding="utf-8") as fr:
        ret = json.load(fr)
    return FlyResponse(data=ret)