# Generated by Django 3.2.6 on 2021-08-25 00:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sparrow', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='addr',
            field=models.CharField(blank=True, default='北京市昌平区', max_length=30, null=True, verbose_name='地址'),
        ),
        migrations.AlterField(
            model_name='student',
            name='age',
            field=models.IntegerField(blank=True, default=18, null=True, verbose_name='年龄'),
        ),
        migrations.AlterField(
            model_name='student',
            name='gold',
            field=models.IntegerField(blank=True, default=100, null=True, verbose_name='金币'),
        ),
        migrations.AlterField(
            model_name='student',
            name='sex',
            field=models.CharField(blank=True, default='男', max_length=10, null=True, verbose_name='性别'),
        ),
    ]
