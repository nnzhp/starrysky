from django.db import models

# Create your models here.
from common.custom_class import BaseModel


class Student(BaseModel):
    name = models.CharField(max_length=30, verbose_name='学生名称')
    sex = models.CharField(max_length=10, verbose_name='性别', default='男',blank=True,null=True)
    age = models.IntegerField(verbose_name='年龄', default=18,blank=True,null=True)
    addr = models.CharField(verbose_name='地址', max_length=30, default='北京市昌平区',blank=True,null=True)
    grade = models.CharField(verbose_name='班级', max_length=30)
    phone = models.CharField(verbose_name='电话', max_length=11,unique=True)
    gold = models.IntegerField(verbose_name='金币', default=100,blank=True,null=True)

    class Meta:
        verbose_name = "学生"
        verbose_name_plural = verbose_name
        db_table = "student"
        ordering = ["-id"]

    def __str__(self):
        return self.name