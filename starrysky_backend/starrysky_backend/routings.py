from django.urls import path
from sparrow import consumers

websocket_urlpatterns = [
    path(r"api/ws_test", consumers.TestConsumer.as_asgi())
]
