class Config:
    default_env = 'Test'  # 当前使用的环境配置，Test

    class Test:
        '''默认测试环境配置'''
        # mysql配置
        mysql_config = {
            "host": "127.0.0.1",  # ip
            "port": 3306,  # 端口号
            "user": "root",  # 用户
            "password": "123456",  # 密码
            "db": "db",  # 数据库
            "autocommit": True,  # 是否自动提交
            "charset": "utf8"  # 字符集
        }
        
        # redis配置
        redis_config = {
            "host": "127.0.0.1",  # ip
            "port": 6379,  # 端口号
            "decode_responses":True,#返回结果是否转成字符串
            "password": None,  # 密码
            "db": 0  # 数据库
        }
        
        # 集群redis配置
        redis_cluster_config = {
            "startup_nodes": [
                {"host": "127.0.0.1", "port": 6379},
                {"host": "127.0.0.1", "port": 6378}
            ],  # 集群的机子
            "decode_responses": True #返回结果是否转成字符串，默认返回的是btyes类型
        }

        server_host = "http://127.0.0.1:8777"  # 服务端接口ip
        user_server_host = "http://127.0.0.1:8777"  # 其他服务的ip
        
    @classmethod
    def get_config(cls, env=default_env):
        if not hasattr(cls,env):
            raise Exception(f"环境{env}不存在！")
        return getattr(cls, env)