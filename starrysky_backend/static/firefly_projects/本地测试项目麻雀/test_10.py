from user import person_info

def test_person_info(get_token):
    result = person_info(get_token,card_type="CC")
    assert result.data.ssn is not None
    
def test_person_info_normal(get_token):
    result = person_info(get_token)
    assert result.data.ssn is not None