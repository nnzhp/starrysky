from config import Config
from utils import Request
project_config = Config.get_config()

def login(username,password):
    '''登录接口'''
    url = project_config.server_host + "/api/user/login"
    data = {"username":username,"password":password}
    req = Request(url,data=data)
    return req.post()
    
def register(phone,email,password,cpassword,nick):
    '''注册接口'''
    url = project_config.server_host + "/api/user/register"
    data = {"phone":phone,"password":password,"nick":nick,"email":email,"password2":cpassword}
    req = Request(url,data=data)
    return req.post()

def person_info(token,bank_code="ICBC",card_type="DC"):
    """四要素生成"""
    url = project_config.server_host + "/api/qa_tools/person_info"
    data = {"bank_code":bank_code,"card_type":card_type}
    req = Request(url,headers={"token":token},params=data)
    return req.get()