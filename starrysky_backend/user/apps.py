from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'user'
    # label = "用户"
    verbose_name = "用户"
