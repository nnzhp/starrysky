from django.conf import settings
import os,platform
from common.log import Log
user_info_key = "user_info:"
user_session_key = "user_session:"
user_session_expire = 60 * 60 * 24 * 1 #1天

if platform.system() == "Linux":
    use_redis = True #判断是否使用redis
    default_host = "http://sky.nnzhp.cn"

else:
    use_redis = False
    default_host = "http://127.0.0.1:8777"
Log.info("use_redis,{},host:{}",use_redis,default_host)
#本地cache文件
cache_file = os.path.join(settings.BASE_DIR,'logs','cache.pickle')

#不需要登录的url
not_login_url = [
    'static','admin','login','register','user_list','student','kabin_info','kexin','calc_game'
]

