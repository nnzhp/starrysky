from django.db import models
from urllib.parse import urljoin
from common.custom_class import BaseModel
from . import const


class UserRole(BaseModel):
    name = models.CharField(verbose_name="权限名称", max_length=50, unique=True)
    alias = models.CharField(verbose_name="权限别名", max_length=50, unique=True)

    class Meta:
        db_table = "user_role"
        verbose_name = "用户角色表"
        verbose_name_plural = verbose_name
        ordering = ["-update_time"]

    def __str__(self):
        return self.name




# Create your models here.
class User(BaseModel):
    phone = models.CharField(verbose_name="手机号", max_length=11, unique=True)
    email = models.EmailField(verbose_name="邮箱", unique=True)
    password = models.CharField(max_length=32, verbose_name="密码")
    nick = models.CharField(max_length=20, verbose_name="昵称")
    avatar = models.ImageField(upload_to='./static/avatar', verbose_name="用户头像", default="static/avatar/default.jpg")
    roles = models.ManyToManyField(UserRole,verbose_name="用户角色",db_constraint=False)

    @property
    def full_avatar_url(self):
        return urljoin(const.default_host, self.avatar.url)

    class Meta:
        db_table = "user"
        verbose_name = "用户表"
        verbose_name_plural = verbose_name
        ordering = ["-update_time"]

    def __str__(self):
        return self.nick
