import itertools
from common.log import Log


class CalcGame:
    chars = ["+", "-", "*", "/"]

    def __init__(self, numbers: list, calc_number: int):
        self.numbers = numbers
        self.calc_number = calc_number
        self.results = []

    def get_groups(self, number):
        return set(itertools.permutations(self.numbers, number))

    def set_calc_result(self, express):
        try:
            if eval(express) == self.calc_number:
                self.results.append(f"{express} = {self.calc_number}")
        except:
            pass

    def get_three_cards_result(self):
        groups = self.get_groups(3)
        for group in groups:
            a, b, c = group
            for f in self.chars:
                for f2 in self.chars:
                    express1 = f"({a} %s {b}) %s {c}" % (f2, f)
                    express2 = f"{a} %s ({b} %s {c})" % (f2, f)
                    express3 = f"{a} %s {b} %s {c}" % (f2, f)
                    self.set_calc_result(express1)
                    self.set_calc_result(express2)
                    self.set_calc_result(express3)

    def get_two_cards_result(self):
        groups = self.get_groups(2)
        for group in groups:
            a, b = group
            for f in self.chars:
                express = f"{a} %s {b}" % f
                self.set_calc_result(express)

    def get_one_cards_result(self):
        if self.calc_number in self.numbers:
            self.results.append(f'{self.calc_number} = {self.calc_number}')

    def get_result(self):
        self.get_three_cards_result()
        self.get_two_cards_result()
        self.get_one_cards_result()
        Log.info("game_results:{}", self.results)
        return self.results


if __name__ == '__main__':
    calc_game = CalcGame([1, 2, 3, 4, 5, 6], 100000)
    calc_game.get_result()
