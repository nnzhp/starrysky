from django.test import TestCase
import requests

host = "http://127.0.0.1:8777"
class TestUser(TestCase):

    def test_register(self):
        data = {
            "phone": "18612532945",
            "email": "admin@qq.com",
            "nick": "单元测试user",
            "password": "123456",
            "password2": "123456"

        }
        response = requests.post(host+'/api/user/register', data=data)

        print(response.json())

    def test_user_list(self):
        response = requests.get(host+'/api/user/user_list')
        print(response.json())

    def test_user_login(self):
        data = {"username":"admin@qq.com","password":"123456"}
        response = requests.post(host+'/api/user/login',data=data)
        print(response.json())