import request from "../utils/request";

export function project_get(data) {

    return request({
        url: "firefly/project",
        method: "get",
        params: data
    })
}

export function project_post(data) {
    return request({
        url: "firefly/project",
        method: "post",
        data
    })
}


export function project_put(data) {
    return request({
        url: "firefly/project",
        method: "put",
        data
    })
}


export function project_delete(data) {
    return request({
        url: "firefly/project",
        method: "delete",
        params: data
    })
}

export function python_file_get(data) {
    return request({
        url: "firefly/python_file",
        method: "get",
        params: data
    })
}

export function python_file_put(data) {
    return request({
        url: "firefly/python_file",
        method: "put",
        data
    })
}

export function python_file_post(data) {
    return request({
        url: "firefly/python_file",
        method: "post",
        data
    })
}


export function python_file_delete(data) {
    return request({
        url: "firefly/python_file",
        method: "delete",
        params: data
    })
}

export function case_get(data) {
    return request({
        url: "firefly/case",
        method: "get",
        params: data
    })
}

export function case_put(data) {
    return request({
        url: "firefly/case",
        method: "put",
        data
    })
}

export function case_post(data) {
    return request({
        url: "firefly/case",
        method: "post",
        data
    })
}

export function case_delete(data) {
    return request({
        url: "firefly/case",
        method: "delete",
        params: data
    })
}

export function case_run(data) {
    return request({
        url: "firefly/case_run",
        method: "get",
        params: data
    })
}

export function case_result_query(data) {
    return request({
        url: "firefly/query_result",
        method: "get",
        params: data
    })
}

export function fixture_get(data) {
    return request({
        url: "firefly/fixture",
        method: "get",
        params: data
    })
}

export function fixture_put(data) {
    return request({
        url: "firefly/fixture",
        method: "put",
        data
    })
}

export function fixture_post(data) {
    return request({
        url: "firefly/fixture",
        method: "post",
        data
    })
}


export function fixture_delete(data) {
    return request({
        url: "firefly/fixture",
        method: "delete",
        params: data
    })
}

export function pip_query(data) {
    return request({
        url: "firefly/pip",
        method: "get",
        params: data
    })
}

export function pip_install(data) {
    return request({
        url: "firefly/pip",
        method: "post",
        params: data
    })
}

export function project_case_run(data) {
    return request({
        url: "firefly/case_run_project",
        method: "post",
        data: data
    })
}


export function query_project_case_record(data) {
    return request({
        url: "firefly/case_run_project",
        method: "get",
        params: data
    })
}

export function query_report_detail(data) {
    return request({
        url: "firefly/query_report_detail",
        method: "get",
        params: data
    })
}