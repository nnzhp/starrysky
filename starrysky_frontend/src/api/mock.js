import request from "../utils/request";

export function public_data_get() {
    return request({
        url: "chameleon/public_param",
        method: "get"
    })
}

export function public_data_update(data) {
    return request({
        url: "chameleon/public_param",
        method: "post",
        data
    })
}

export function category_get(data) {
    return request({
        url: "chameleon/interface_category",
        method: "get",
        params:data
    })
}

export function category_put(data) {
    return request({
        url: "chameleon/interface_category",
        method: "put",
        data
    })
}

export function category_post(data) {
    return request({
        url: "chameleon/interface_category",
        method: "post",
        data
    })
}

export function category_delete(data) {
    return request({
        url: "chameleon/interface_category",
        method: "delete",
        params:data
    })
}

export function interface_get(data) {
    return request({
        url: "chameleon/interface",
        method: "get",
        params:data
    })
}

export function interface_put(data) {
    return request({
        url: "chameleon/interface",
        method: "put",
        data
    })
}

export function interface_post(data) {
    return request({
        url: "chameleon/interface",
        method: "post",
        data
    })
}
export function interface_batch_delete(data) {
    return request({
        url: "chameleon/interface_batch_delete",
        method: "post",
        data
    })
}

export function interface_delete(data) {
    return request({
        url: "chameleon/interface",
        method: "delete",
        params:data
    })
}