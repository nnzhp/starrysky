import request from "../utils/request";

export function encrypt_decrypt(data) {
    return request({
        url: "/qa_tools/encrypt_decrypt",
        method: "post",
        data
    })
}

export function create_sign(data) {
    return request({
        url: "/qa_tools/create_sign",
        method: "post",
        data
    })
}