import Vue from "vue";
import encrypt_decrypt from './views/qa_tools/encrypt_decrypt'
import create_sign from './views/qa_tools/create_sign'
import category from './views/mock/category'
import layout from './components/layout'
import interface2 from './views/mock/interface'
import {Message} from "element-ui";
import page_404 from './views/page_404'
import login from './views/user/login'
import register from './views/user/register'
import calc_game from './views/user/calc_game'
import public_data from './views/mock/public_data'
import mock_index from './views/mock/index'
import user_admin from './views/user/user_admin'
import create_qrcode from './views/qa_tools/create_qrcode'
import person_info from './views/test_data/person_info'
import mock_data from './views/test_data/mock_data'
import VueRouter from "vue-router"
import tools from "./utils/tools"
import project from "./views/firefly/project"
import project_edit from "./views/firefly/project_edit"
import interface_code from "./views/firefly/interface_code"
import interface_code_edit from "./views/firefly/interface_code_edit"
import firefly_index from "./views/firefly/firefly_index"
import fixture from "./views/firefly/fixture"
import fixture_edit from "./views/firefly/fixture_edit"
import test_case from "./views/firefly/case"
import case_edit from "./views/firefly/case_edit"
import case_result from "./views/firefly/case_result"
import pip_install from "./views/firefly/pip_install"
import batch_run_case from "./views/firefly/batch_run_case"
import case_result_project from "./views/firefly/case_result_project"


Vue.use(VueRouter);

const routes = [
    {
        path: '/firefly',
        name: "接口测试",
        redirect: "/firefly/index",
        component: layout,
        meta: {"title": "接口测试"},

        children: [
            {
                path: "index",
                component: firefly_index,
                name: "firefly_index",
                meta: {"title": "接口统计", "icon": "el-icon-s-data"}
            },
            {
                path: "help",
                name: "help",
                meta: {
                    "title": "使用说明",
                    "icon": "el-icon-question",
                    link: "https://www.yuque.com/docs/share/38e2cff3-f03d-4103-b232-0b16ec10d73c"
                }
            },
            {
                path: "project",
                component: project,
                name: "project",
                meta: {"title": "项目配置", "icon": "el-icon-monitor", cache: false}
            },
            {
                path: "project_edit",
                component: project_edit,
                name: "project_edit",
                meta: {"title": "项目编辑", is_nav: false, cache: false}
            },
            {
                path: "interface_code",
                component: interface_code,
                name: "interface_code",
                meta: {"title": "接口管理", "icon": "el-icon-connection", cache: false}
            },
            {
                path: "interface_edit",
                component: interface_code_edit,
                name: "interface_edit",
                meta: {"title": "接口编辑", is_nav: false, cache: false}
            },
            {
                path: "fixture",
                component: fixture,
                name: "fixture",
                meta: {"title": "fixture管理", "icon": "el-icon-watermelon", cache: false}
            },
            {
                path: "fixture_edit",
                component: fixture_edit,
                name: "fixture_edit",
                meta: {"title": "fixture编辑", is_nav: false, cache: false}
            },
            {
                path: "test_case",
                component: test_case,
                name: "test_case",
                meta: {"title": "case管理", "icon": "el-icon-suitcase-1", cache: false}
            },
            {
                path: "case_edit",
                component: case_edit,
                name: "case_edit",
                meta: {"title": "case编辑", is_nav: false, cache: false}
            },
            {
                path: "case_result",
                component: case_result,
                name: "case_result",
                meta: {"title": "case结果", is_nav: false, cache: false}
            },
            {
                path: "pip_install",
                component: pip_install,
                name: "pip_install",
                meta: {"title": "模块安装", "icon": "el-icon-s-claim"}
            },
            {
                path: "case_run_batch",
                component: batch_run_case,
                name: "batch_run_case",
                meta: {"title": "批量运行", "icon": "el-icon-school"}
            },
            {
                path: "case_result_project",
                component: case_result_project,
                name: "case_result_project",
                meta: {"title": "测试报告", is_nav: false, cache: false}
            },


        ]
    },
    {
        path: '/mock',
        name: "mock",
        redirect: "/mock/index",
        component: layout,
        meta: {"title": "mock平台"},

        children: [
            {
                path: "index",
                component: mock_index,
                name: "mock_index",
                meta: {"title": "使用说明", "icon": "el-icon-document"}
            },
            {
                path: "public_data",
                component: public_data,
                name: "public_data",
                meta: {"title": "公共参数", "icon": "el-icon-setting"}
            },

            {
                path: "category",
                component: category,
                name: "category",
                meta: {"title": "接口分类", "icon": "el-icon-menu"}
            },
            {
                path: "interface",
                component: interface2,
                name: "interface",
                meta: {"title": "接口管理", "icon": "el-icon-paperclip"}
            },
            // {
            //     path: "baidu",
            //     name: "baidu",
            //     meta: {"title": "百度", "icon": "el-icon-paperclip", link: "http://www.baidu.com"}
            // },
        ]
    },
    {
        path: '/qa_tools', component: layout, name: "qa_tools",
        meta: {"title": "测试工具"},
        children: [
            {
                path: "",
                component: create_qrcode,
                name: "create_qrcode",
                meta: {"title": "二维码生成", "icon": "el-icon-mobile-phone"}
            },
            {
                path: "encrypt_decrypt",
                component: encrypt_decrypt,
                name: "encrypt_decrypt",
                meta: {"title": "数据加解密", "icon": "el-icon-lock"}
            },
            {
                path: "create_sign",
                component: create_sign,
                name: "create_sign",
                meta: {"title": "签名计算", "icon": "el-icon-edit"}
            }
        ],
    },
    {
        path: '/data', component: layout,
        meta: {"title": "数据中心"},
        children: [
            {
                path: "",
                component: person_info,
                name: "data",
                meta: {"title": "四要素生成", "icon": "el-icon-s-custom"}
            },
            {
                path: "mock_data",
                component: mock_data,
                name: "mock_data",
                meta: {"title": "接口数据构造", "icon": "el-icon-bank-card"}
            },
        ],
    },
    {
        path: '/user_admin', component: layout,
        children: [{
            path: "",
            component: user_admin,
            name: "user_admin",
            meta: {"title": "新增用户", "icon": "el-icon-setting"}
        }],
        meta: {"title": "用户管理", "roles": ["qa"]}
    },
    {path: '*', name: "page_404", component: page_404, meta: {is_nav: false}},
    {path: '/', name: "index", redirect: "/firefly", meta: {is_nav: false}},
    {path: '/login', name: "login", component: login, meta: {is_nav: false}},
    {path: '/calc_game', name: "calc_game", component: calc_game, meta: {is_nav: false}},
    {path: '/register', name: "register", component: register, meta: {is_nav: false}},
    {path: '/doc', name: "apidoc", meta: {title: "接口文档", link: "http://sky.nnzhp.cn/doc/index.html"}},
]

var router = new VueRouter({routes})

router.beforeEach((to, from, next) => {
    if (to.path !== "/login" && to.path !== "/register" && to.path !== "/calc_game") {
        var token = tools.get_token()
        if (token) {
            next()
        } else {
            Message({
                message: "token失效，请重新登录！",
                type: 'error',
                duration: 5 * 1000
            })
            router.push('/login')
        }
    } else {
        next()
    }


})
export default router