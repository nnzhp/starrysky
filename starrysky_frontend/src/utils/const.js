export default {
    // server_host:"http://127.0.0.1:8777",
    server_host:"http://sky.nnzhp.cn",
    title: "星瀚",
    page_size: 20,
    page_range: [20, 50, 200, 400],
    mock_request_methods: {"GET": 0, "POST": 1, "PUT": 2, "DELETE": 3},
    rsa_type: [{name: "加密", id: 1}, {name: "解密", id: 2}],
    bank_list: [
        {"code": "ABC", "name": "中国农业银行"},
        {"code": "CEB", "name": "中国光大银行"},
        {"code": "CITIC", "name": "中信银行"},
        {"code": "SHBANK", "name": "上海银行"},
        {"code": "SPDB", "name": "上海浦东发展银行"},
        {"code": "CMB", "name": "招商银行"},
        {"code": "ICBC", "name": "中国工商银行"},
        {"code": "BOC", "name": "中国银行"},
        {"code": "CCB", "name": "中国建设银行"},
        {"code": "PSBC", "name": "中国邮政储蓄银行"},
        {"code": "QHDBANK", "name": "秦皇岛银行"},
        {"code": "SPABANK", "name": "平安银行"},
        ],
    "bank_card_type":[
        {code:"CC",name:"借记卡"},
        {code:"DC",name:"信用卡"},
        ],
    project_init_code:`class Config:
    default_env = 'Test'  # 当前使用的环境配置，Test

    class Test:
        '''默认测试环境配置'''
        # mysql配置
        mysql_config = {
            "host": "127.0.0.1",  # ip
            "port": 3306,  # 端口号
            "user": "root",  # 用户
            "password": "123456",  # 密码
            "db": "db",  # 数据库
            "autocommit": True,  # 是否自动提交
            "charset": "utf8"  # 字符集
        }
        
        # redis配置
        redis_config = {
            "host": "127.0.0.1",  # ip
            "port": 6379,  # 端口号
            "decode_responses":True,#返回结果是否转成字符串
            "password": None,  # 密码
            "db": 0  # 数据库
        }
        
        # 集群redis配置
        redis_cluster_config = {
            "startup_nodes": [
                {"host": "127.0.0.1", "port": 6379},
                {"host": "127.0.0.1", "port": 6378}
            ],  # 集群的机子
            "decode_responses": True #返回结果是否转成字符串，默认返回的是btyes类型
        }

        server_host = "http://127.0.0.1:8777"  #服务端接口ip
        user_server_host = "http://127.0.0.1:8777"  # 其他变量
        
    @classmethod
    def get_config(cls, env=default_env):
        if env == None:
            return getattr(cls, cls.default_env)
        if not hasattr(cls,env):
            raise Exception(f"环境{env}不存在！")
        return getattr(cls, env)   
    `,

}