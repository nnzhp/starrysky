import axios from 'axios'
import {MessageBox, Message} from 'element-ui'
import tools from "./tools";
import router from "../router";

const service = axios.create({
    baseURL: '/api', // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 50000 // request timeout
})


//请求拦截器
service.interceptors.request.use(
    config => {
        config.headers['token'] = tools.get_token()
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// 返回拦截器
service.interceptors.response.use(
    response => {
        const res = response.data
        // if the custom code is not 20000, it is judged as an error.
        if (res.code === 405) {
            MessageBox.confirm("登录过期，点击确认跳转到登录页面", "确认退出").then(() => {
                tools.remove_token()

                router.push('/login')
            })
        }
        if (res.code !== 0) {
            Message({
                message: res.msg || 'Error',
                type: 'error',
                duration: 5 * 1000
            })

        } else {
            return res
        }
    },
    error => {

        console.log('err' + error) // for debug
        Message({
            message: error.msg,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(error)
    }
)

export default service