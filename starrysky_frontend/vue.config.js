module.exports = {
    publicPath: '/',
    outputDir: 'dist',
    assetsDir: 'static_sky',
    devServer: {
        port: 9999,
        open: true,
        overlay: {
            warnings: false,
            errors: true
        },
        proxy: {
            "/api": {
                // target: "http://sky.nnzhp.cn/",
                target: "http://127.0.0.1:8777/",
                changeOrigin: true,
            },
        },
    },
}